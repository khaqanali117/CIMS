@if (session('success'))
    <br>
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
@if (session('error'))
    <br>
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif