<!--start side menu bar -->
<aside id="sidebar" class="sidebar c-overflow">
    <div class="s-profile">
        <a href="{{ route('adminIndex') }}" data-ma-action="profile-menu-toggle">
            <div class="sp-pic">
                <img src="{{ asset('dashboard/uploads/profile-pictures/users/'.Auth::user()->id.'/'.Auth::user()->profile_image) }}"
                     alt="Profile Picture" onerror="this.src='{{ asset('dashboard/uploads/profile-pictures/users/default/2.jpg') }}'">
            </div>

            <div class="sp-info">
                {{ Auth::user()->first_name.' '.Auth::user()->last_name }}
                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>
        <ul class="main-menu">
            <li>
                <a href="#/user-profile"><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a href="#/change-password"><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
            <li>
                <a href="{{ route('getLogout') }}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>
        </ul>
    </div>

    <ul class="main-menu">
        <li class="active">
            <a href="{{ route('adminIndex') }}"><i class="zmdi zmdi-home"></i> Home</a>
        </li>
        <!-- Accounts -->
        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-accounts"></i>
                Accounts
            </a>
            <ul>
                <li><a href="#/users-list">Users</a></li>
                <li><a href="#/supplier-list">Suppliers</a></li>
                <li><a href="#/customer-list">Customers</a></li>
            </ul>
        </li>
        <!-- Inventory -->
        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-assignment"></i>
                Inventory
            </a>
            <ul>
                <li>
                    <a href="#/inventory-list">Inventory</a>
                </li>
                <li>
                    <a href="#/menu-list">Menu items</a>
                </li>
                <li>
                    <a href="#/deals-list">Deals</a>
                </li>
                <li><a href="#/category-list">Manage Categories</a></li>
            </ul>
        </li>
        <!-- Orders -->
        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-case"></i>
                Orders
            </a>
            <ul>
                <li>
                    <a href="#/order-list">Order list</a>
                </li>
                <li>
                    <a href="#/indoor-order">Indoor Order</a>
                </li>
                <li>
                    <a href="#/phone-order">Phone Order</a>
                </li>
            </ul>
        </li>
        <!-- Sales -->
        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-money-box"></i>
                Sales
            </a>
            <ul>
                <li>
                    <a href="#/test"></a>
                </li>
            </ul>
        </li>

        <!-- Orders -->
        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-case"></i>
                Purchases
            </a>
            <ul>
                <li>
                    <a href="#/add-purchase">Add new</a>
                </li>
                <li>
                    <a href="#/purchase-form">purchase List</a>
                </li>

            </ul>
        </li>

    </ul>
</aside>
<!-- end side menu bar