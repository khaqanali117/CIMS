@include('dashboard.include.header')
<section id="main">
    @include('dashboard.include.SideMenuBar')
    @include('dashboard.include.ChatBar')
    <section id="content">
        @yield('content')
    </section>
</section>
@include('dashboard.include.footer')