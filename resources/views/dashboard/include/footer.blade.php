<!--start footer-->
<footer id="footer">
    Copyright &copy; 2015 Material Admin
    <ul class="f-menu">
        <li><a href="index.html">Home</a></li>
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="index.html">Reports</a></li>
        <li><a href="index.html">Support</a></li>
        <li><a href="index.html">Contact</a></li>
    </ul>
</footer>
<!-- end footer-->
<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>
        <p>Please wait...</p>
    </div>
</div>
<!--Models -->
<div class="modal fade" id="modal-data" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" id="modal-size-content"></div>
</div>
<!-- Script Start -->
@section('scripts')
    @include('dashboard.include.JavaScript')
@show
</body>
</html>
