<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CIMS - Login</title>
    <link href="{{elixir('dashboard/css/all.css') }}" rel="stylesheet">

</head>
<body>
    <div class="login-content">
        <!-- Login -->
        <div class="lc-block toggled" id="l-login">
            @if(session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
            <form method="post" action="{{ route('postLogin') }}">
                {{ csrf_field() }}
                <div class="lcb-form">
                    <div class="input-group m-b-20 ">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line {{ $errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Email Or Phone#" name="username">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                        <div class="fg-line {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                        </div>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember">
                            <i class="input-helper"></i>
                            Keep me signed in
                        </label>
                    </div>
                    <button class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
                </div>
            </form>
            <div class="lcb-navigation">
                <a href="#l" data-ma-action="login-switch" data-ma-block="#l-forget-password">
                    <i>?</i>
                    <span>Forgot Password</span>
                </a>
            </div>
        </div>
        <!-- Forgot Password -->
        <div class="lc-block" id="l-forget-password">
            <div class="lcb-form">
                <p class="text-left">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.
                </p>
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="fg-line">
                        <input type="text" class="form-control" placeholder="Email Address">
                    </div>
                </div>
                <a href="#" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
            </div>
            <div class="lcb-navigation">
                <a href="#" data-ma-action="login-switch" data-ma-block="#l-login">
                    <i class="zmdi zmdi-long-arrow-right"></i>
                    <span>Sign in</span>
                </a>
            </div>
        </div>
    </div>
    <!-- Javascript Libraries -->
    <script src="{{ elixir('dashboard/js/all.js') }}"></script>
</body>
</html>
