<?php

return [
    'success' => [
        'create'        => ':name Saved Successfully',
        'update_deal'   => 'Deal Information Updated Successfully',
        'delete_deal'   => 'Deal Information Deleted Successfully',
        'not_found'     => ':name not found',
    ],
    'error' => [
        'required' => ':name is required'
    ]
];