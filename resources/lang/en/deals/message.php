<?php

return [
    'required' =>[
        'deal_name' => 'Deal Title is required',
        'sale_price' => 'Deal sale price is required'
    ],
    'success' => [
        'new_deal'      => 'New Deal Save Successfully',
        'update_deal'   => 'Deal Information Updated Successfully',
        'delete_deal'   => 'Deal Information Deleted Successfully',
    ],
    'error' => [

    ]

];
