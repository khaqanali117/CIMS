/*
 Angular routes See all users
 */
app.config(function($routeProvider){
    /*
     route for Dashboard
     */
    $routeProvider.when('/',{
        templateUrl:'/dashboard/templates/dashboard.html',
        controller: 'dashboardContent'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
* User Routes
****************************************************************************************/
    /*
     route for all users
     */
    $routeProvider.when('/users-list',{
        templateUrl:'/dashboard/templates/users/all-users.html',
        controller: 'usersList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     route for user profile
     */
    $routeProvider.when('/user-profile',{

        templateUrl:'/dashboard/templates/users/profile.html',
        controller: 'userProfile'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    $routeProvider.when('/change-password',{

        templateUrl:'/dashboard/templates/users/changePassword.html',
        controller: 'userProfile'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
 * Supplier Routes
 ****************************************************************************************/
    /*
     route for all suppliers
     */
    $routeProvider.when('/supplier-list',{
        templateUrl:'/dashboard/templates/supplier/supplier-list.html',
        controller: 'supplierList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
* Customer Routes
****************************************************************************************/
    /*
     route for all customers
     */
    $routeProvider.when('/customer-list',{
        templateUrl:'/dashboard/templates/customer/customer-list.html',
        controller: 'customerList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
* Inventory Routes
****************************************************************************************/
    /*
     route for Inventory Items
     */
    $routeProvider.when('/inventory-list',{
        templateUrl:'/dashboard/templates/inventory/inventory-list.html',
        controller: 'inventoryList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     route for new Inventory Items
     */
    $routeProvider.when('/inventory-item',{
        templateUrl:'/dashboard/templates/inventory/inventory-item.html',
        controller: 'inventoryItem'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
 * Menu Item Routes
 ****************************************************************************************/
    /*
     route for Menu Items
     */
    $routeProvider.when('/menu-list',{
        templateUrl:'/dashboard/templates/menu/menu-list.html',
        controller: 'menuList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
 * Deals Routes
 ****************************************************************************************/
    /*
     route for Deals
     */
    $routeProvider.when('/deals-list',{
        templateUrl:'/dashboard/templates/deals/deals-list.html',
        controller: 'dealsController'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
    * Add new deal
    */
    $routeProvider.when('/deal-add',{
        templateUrl:'/dashboard/templates/deals/deal-add.html',
        controller: 'newDealController'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     * Add new deal
     */
    $routeProvider.when('/deal-update/:item',{
        templateUrl:'/dashboard/templates/deals/deal-update.html',
        controller: 'updateDealController'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
* Category Routes
****************************************************************************************/
    /*
     route for category list
     */
    $routeProvider.when('/category-list',{
        templateUrl:'/dashboard/templates/category/category-list.html',
        controller: 'categoryList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
/***************************************************************************************
 * Order Routes
 ****************************************************************************************/
    /*
     route for order list
     */
    $routeProvider.when('/order-list',{
        templateUrl:'/dashboard/templates/order/order-list.html',
        controller: 'orderList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     route for indoor order
     */
    $routeProvider.when('/indoor-order',{
        templateUrl:'/dashboard/templates/order/indoor-order.html',
        controller: 'orderController'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     route for phone order
     */
    $routeProvider.when('/phone-order',{
        templateUrl:'/dashboard/templates/order/phone-order.html',
        controller: 'orderController'
    })
        .otherwise({ redirectTo: 'templates/404.html' });
    /*
     route for order list
     */
    $routeProvider.when('/order-list',{
        templateUrl:'/dashboard/templates/order/order-list.html',
        controller: 'orderList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });

    /*
     route for order list
     */
    $routeProvider.when('/add-purchase',{
        templateUrl:'/dashboard/templates/purchase/new-purchase.html',
        controller: 'purchaseList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });

    /*
     route for Purchase list
     */
    $routeProvider.when('/purchase-form',{
        templateUrl:'/dashboard/templates/purchase/purchase-list.html',
        controller: 'purchaseList'
    })
        .otherwise({ redirectTo: 'templates/404.html' });

});