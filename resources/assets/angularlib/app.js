var app = angular.module('admin',['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'autocomplete'],function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});