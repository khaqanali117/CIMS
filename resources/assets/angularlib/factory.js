app.factory('commonFunction', function () {
        return{
            Error_function : function(message){
                var html ='<div class="alert alert-danger"><strong>';
                $.each(message, function(index, value) {
                   html += '<p class="text-left">'+ value +'</p>'
               });
               html += '</strong></div>';
               return html;
            },
            Success_function: function(message){
                return '<div class="alert alert-success"><strong><p class="text-left">'+ message +'</p></strong></div>';
            },
            RemoveMessage:function () {
              $('#messages').empty();
            },
            /*
            * Type will be:
            * inverse, info, success, warning, danger
            * */
            Toast:function (message,type) {
                $.growl({
                   title: ' '+ type + ' ',
                   message: message,
                   url: ''
                },{
                   element: 'body',
                   type: type,
                   allow_dismiss: true,
                   offset: {
                       x: 20,
                       y: 85
                   },
                   spacing: 10,
                   z_index: 1100,
                   delay: 2500,
                   timer: 1000,
                   url_target: '_blank',
                   mouse_over: false,
                   icon_type: 'class',
                   template: '<div data-growl="container" class="alert" role="alert">' +
                   '<button type="button" class="close" data-growl="dismiss">' +
                   '<span aria-hidden="true">&times;</span>' +
                   '<span class="sr-only">Close</span>' +
                   '</button>' +
                   '<span data-growl="icon"></span>' +
                   '<span data-growl="message"></span>' +
                   '<a href="#" data-growl="url"></a>' +
                   '</div>'
                });
            },
            /*
            * Active Loader
            */
            ActiveLoader:function () {
                var loader = angular.element(document.getElementsByClassName('page-loader'));
                loader.addClass('request-loader');
            },
            /*
            * Stop Loader
            */
            StopLoader:function () {
                var loader = angular.element(document.getElementsByClassName('page-loader'));
                loader.removeClass('request-loader');
            },
            /*
            * Modal
            * Class will be 'modal-sm','modal-lg'
            * In case of default modal send boolean(false) in modalSize
            * If we want to load data from external html form then please used isExternalFile true
            */
            showModal : function (modalObj, modalSize, modalBody, modalController, callBack) {
                var modalInstance = modalObj.open({
                    animation: true,
                    templateUrl: modalBody,
                    controller: modalController,
                    controllerAs: true,
                    size: modalSize?modalSize:''
                });
                if(callBack)
                    callBack(modalInstance)
            },
            /*
             * Initialize core libs
             */
            initializeCoreLib:function () {
                $('.selectpicker').selectpicker();
                $('.date-picker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.time-picker').datetimepicker({
                    format: 'LT'
                });
            },
            /*
            * calculate price
            */
            calculatePrice:function (quantity, price) {
                if((quantity && price) && (quantity > 0 && price > 0))
                    return quantity * price;
                else
                    return false;
            },
            /*
             * calculate discount
             * Formula
             * Discount  =  List Price × Discount Rate/100
             */
            calculateDiscount:function (discount, price) {
                if((discount && price) && (discount > 0 && price > 0)) {
                    var dis = price * (discount / 100);
                    var price = price - dis;
                    return {save:dis,price:price};
                }
                else
                    return false;
            },
        }
    });
/**
 * Notifications Dialogs
 * Type will be:
 * inverse, info, success, warning, danger
 * Third paramet will be the function name which do you want to excute on confirm
*/
app.factory('Dialog',function () {
    return{
        Success : function (title, message) {
            swal({
                title: title,
                text: message,
                type: "success",
                timer: 2000,
                showConfirmButton: false
            });
        },
        Warning : function (title, message, func) {
            swal({
                title: title,
                text: message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(isConfirm){
                if (isConfirm) {
                    swal("Deleted!", "", "success");
                    func();
                }
            });
        }
    }
});

/*
* Data sharing between controllers
*/
app.factory('sharedProperties', function(){
   var data = {};

   return {
       setValue: function (key,val){
           data[key] = val;
       },
       getValue: function (key){
           return data[key];
       }
   }
});

