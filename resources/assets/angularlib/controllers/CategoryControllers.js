/***************************************************************************************
 * Category Controller
 ****************************************************************************************/
/*
 Controller for Category list
 */
app.controller('categoryList',function($scope, $http, $uibModal, commonFunction, Dialog, requestService){
    var category_id = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get Category data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/category/list', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.categories = responseData.data;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    /*
    * load new inventory model
    */
    $scope.categoryModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/category/category-add.html', 'newCategoryController', false);
    };
    /*
     * Edit category item information
     */
    $scope.updateCategory = function (item) {
        $scope.index = $scope.categories.indexOf(item);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/category/category-update.html', 'updateCategoryController', false);
    };
    /*
     * Delete category item information
     */
    $scope.deleteCategory = function (cat_id, item) {
        category_id = cat_id;
        $scope.index = $scope.categories.indexOf(item);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You want to delete this', deleteCategoryRequest);
    };
    /*
     * Delete category item request
     */
    var deleteCategoryRequest = function() {
        //active loader
         commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/category/delete/'+category_id, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.categories.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * save new category information
 */
app.controller('newCategoryController', function ($scope, commonFunction, requestService) {

    $scope.saveCategory = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.categories.push(responseData.data);
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveCategory')[0].reset();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update category information
 */
app.controller('updateCategoryController', function ($scope, commonFunction, requestService) {
    /*
     * Get previous data
     */
    $scope.categories = angular.copy($scope.$$prevSibling.categories);
    $scope.category_info = $scope.categories[$scope.$$prevSibling.index];

    $scope.updateCategory = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.categories[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
