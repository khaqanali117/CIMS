/***************************************************************************************
 * User Controller
 ****************************************************************************************/
/*
 Controller for all user page
 */
app.controller('userProfile',function($scope,$http, $httpParamSerializerJQLike, commonFunction, fileUpload, requestService){
    $scope.user_data = {};
    $scope.formdata = {};
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get user data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/user/userProfile', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.user_info = responseData.data;
            $scope.user_data.first_name = $scope.user_info.first_name;
            $scope.user_data.last_name = $scope.user_info.last_name;
            $scope.first_name = $scope.user_info.first_name;
            $scope.last_name = $scope.user_info.last_name;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    //update basic info
    $scope.updateBasicInfo = function () {
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+'user/userProfile', $httpParamSerializerJQLike($scope.user_data) , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.first_name = $scope.user_data.first_name;
                    $scope.last_name = $scope.user_data.last_name;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast(responseData.message, 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    };
    //update profile picture
    $scope.updateProfilePic = function (event) {
        //active loader
        commonFunction.ActiveLoader();
        var file = event.target.files[0];
        var type = file.type;
        if(type != 'image/jpeg' && type != 'image/png') {
            commonFunction.Toast('Please select a valid image file..', 'danger');
            //stop loader
            commonFunction.StopLoader();
            return false;
        }
        var uploadUrl = JS_Base_Path+"/user/userProfile";
        fileUpload.uploadFileToUrl(file, uploadUrl, function (response) {
            if(response.isResponse) {
                commonFunction.Toast(response.message, 'success');
                setInterval(function () {
                    window.location.reload();
                }, 2000);
            }
            else
                commonFunction.Toast(response.message,'danger');
        });
        //stop loader
        commonFunction.StopLoader();
    }
    //update password model
    $scope.processForm = function () {
        //active loader
        commonFunction.ActiveLoader();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+'/user/userProfile', $httpParamSerializerJQLike($scope.formdata) , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse)
                    commonFunction.Toast(responseData.message, 'success');
                else
                    commonFunction.Toast(responseData.message, 'danger');

            }else {
                commonFunction.Toast('Oops!Something Went Wrong..','danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});