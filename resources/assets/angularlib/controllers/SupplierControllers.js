/***************************************************************************************
 * Supplier Controller
 ****************************************************************************************/
/*
 Controller for supplier page
 */
app.controller('supplierList',function($scope,$http, $uibModal, commonFunction, Dialog, requestService){
    var supplier_id = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get supplier data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/supplier/getSuppliers', function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.suppliers = responseData.suppliers_info;
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
    //new supplier model
    $scope.supplierModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/supplier/new-supplier.html', 'newSupplierController', false);
    };
    //update supplier model
    $scope.updateSupplier = function (supplier) {
        $scope.index = $scope.suppliers.indexOf(supplier);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/supplier/supplier-update.html', 'updateSupplierController', false);
    };
    /*
     * Delete supplier information
     */
    $scope.deleteSupplier = function (id, supplier) {
        supplier_id = id;
        $scope.index = $scope.suppliers.indexOf(supplier);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You Want To Delete This User', deleteSupplierRequest);
    };
    /*
     * Delete supplier request
     */
    var deleteSupplierRequest = function() {
        //active loader
        commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/supplier/deleteSupplier/'+supplier_id, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.suppliers.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});

/*
 * save new supplier information
 */
app.controller('newSupplierController', function ($scope, commonFunction, requestService) {

    $scope.saveSupplier = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.suppliers.push(responseData.data);
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveSupplier')[0].reset();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update supplier information
 */
app.controller('updateSupplierController', function ($scope, commonFunction, requestService) {
    /*
     * Get previous data
     */
    $scope.suppliers = angular.copy($scope.$$prevSibling.suppliers);
    $scope.supplier_info = $scope.suppliers[$scope.$$prevSibling.index];

    $scope.updateSupplier = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.suppliers[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});