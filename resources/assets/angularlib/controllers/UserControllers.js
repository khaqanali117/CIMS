/***************************************************************************************
 * User Controller
 ****************************************************************************************/
/*
 Controller for all user page
 */
app.controller('usersList',function($scope,$http, $uibModal,commonFunction, Dialog, requestService){
    var user_id = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get users data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/user/getUsers', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.users = responseData.user_info;
            $scope.roles = responseData.user_roles;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    //new user model
    $scope.userModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/users/new-user.html', 'newUserController', false);
    };
    //update user model
    $scope.updateUser = function (user) {
        $scope.index = $scope.users.indexOf(user);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/users/update-user.html', 'updateUserController', false);
    };
    /*
     * Delete user information
     */
    $scope.deleteUser = function (id, user) {
        user_id = id;
        $scope.index = $scope.users.indexOf(user);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You Want To Delete This User', deleteUserRequest);
    };
    /*
     * Delete user request
     */
    var deleteUserRequest = function() {
        //active loader
        commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/user/deleteUser/'+user_id, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.users.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
* save new user information
*/
app.controller('newUserController', function ($scope, $httpParamSerializerJQLike, commonFunction, requestService) {
    $scope.newUserData = {};
    //active loader
    commonFunction.ActiveLoader();
    //call get request method from service
    requestService.httpGetAjax(JS_Base_Path+'/user/createUser', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.roles = responseData.data;
            console.log($scope.$$prevSibling.users);
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });

    $scope.saveUser = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $httpParamSerializerJQLike($scope.newUserData) , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.users.push(responseData.data);
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveUser')[0].reset();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update user information
 */
app.controller('updateUserController', function ($scope, commonFunction, requestService) {
    /*
    * Get previous data
    */
    $scope.users = angular.copy($scope.$$prevSibling.users);
    $scope.user_info = $scope.users[$scope.$$prevSibling.index];
    $scope.roles = $scope.$$prevSibling.roles;

    $scope.updateUser = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.users[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});