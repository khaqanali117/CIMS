/***************************************************************************************
 * Menu Controller
 ****************************************************************************************/
/*
 Controller for Menu list
 */
app.controller('menuList',function($scope, $http, $uibModal, commonFunction, Dialog, requestService){
    var menuId = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get menu data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/menu/list', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.menuItems = responseData.data;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    /*
     * get products
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/menu/create', function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.products = responseData.data
    });
    /*
    * load new menu model
    */
    $scope.menuModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/menu/menu-add.html', 'newMenuController', false);
    };
    /*
     * Edit inventory item information
     */
    $scope.updateMenu = function (item) {
        $scope.index = $scope.menuItems.indexOf(item);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/menu/menu-update.html', 'updateMenuController', false);
    };
    /*
     * menu component
     */
    angular.element(document).ready(function () {
        //add new component row
        $(document).on('click', '.add-component', function () {
            var parents_el = $(this).parents('.menu-component');
            var clone_html = parents_el.clone();
            clone_html.find('.quantity').val('');
            clone_html.find('.dropdown-toggle').remove();
            clone_html.find('.dropdown-menu').remove();
            parents_el.after(clone_html);
            commonFunction.initializeCoreLib();
            $('.menu-component').not(':first').find('.remove-component').removeClass('hide');
        });
        //remove component row
        $(document).on('click', '.remove-component', function () {
           $(this).parents('.menu-component').remove();
        });
    });

    /*
     * Delete menu item information
     */
    $scope.deleteMenu = function (menu_id, item) {
        menuId = menu_id;
        $scope.index = $scope.menuItems.indexOf(item);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You want to delete this', deleteMenuRequest);
    };

    /*
     * Delete Menu item request
     */
    var deleteMenuRequest = function() {
        //active loader
         commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/menu/delete/'+menuId, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.menuItems.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});


/*
 * save new menu information
 */
app.controller('newMenuController', function ($scope, commonFunction, requestService) {

    $scope.products = $scope.$$prevSibling.products;

    $scope.saveMenu = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.menuItems.push(responseData.data);
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveMenu')[0].reset();
                    $('.menu-component').not(':first').remove();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update menu information
 */
app.controller('updateMenuController', function ($scope, commonFunction, requestService) {
    /*
     * Get previous data
     */
    $scope.menuItems = angular.copy($scope.$$prevSibling.menuItems);
    $scope.menuItem_info = $scope.menuItems[$scope.$$prevSibling.index];
    $scope.products = $scope.$$prevSibling.products;

    $scope.updateMenu = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.menuItems[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
