/***************************************************************************************
 * Order Controller
 ****************************************************************************************/

app.controller('orderList',function ($scope,$http, $uibModal, commonFunction, requestService, sharedProperties, $filter) {
    //active loader
    commonFunction.ActiveLoader();
    /*
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'order/orderList', function (responseData, responseType) {
        // console.log(responseData);
        if(responseData.isResponse) {
            if(responseData.data){
                $scope.ordersList = dataArrange(responseData.data);
                // $scope.ordersList = responseData.data;
            }
            else
                commonFunction.Toast(responseData.message, 'info');
        }
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
    /*
     * calculate Total
     */
    $scope.updateOrder = function (order) {
        sharedProperties.setValue('editOrder', order);
    };
    /*
     * calculate Total
     */
    $scope.calculateTotal = function (quantity, price, discount) {
        var total = commonFunction.calculatePrice(quantity, price);
        var after_dis = commonFunction.calculateDiscount(discount, total);
        if(after_dis)
            return after_dis;

        return total;
    };

    /*-- Rearrange data --*/
    var dataArrange = function (data) {
        var arrangeData = [];
        var temData = [];
        angular.forEach(data,function (val, key) {
            temData = [];
           var result = $filter("filter")(data, function (item) {
                if(item.order_id == val.order_id) {
                    temData.push({
                        name        :item.product_name?item.product_name:item.title?item.title:item.deal_name,
                        quantity    :item.quantity,
                        sale_price  :item.sale_price,
                        sale_dis    :item.item_discount?item.item_discount:0,
                        item_type   :item.item_type
                    });
                    return true;
                }
                return false;
            });
           if(result.length > 0){
               arrangeData.push({
                   order_id            : val.order_id,
                   mobile              : val.mobile,
                   table_no            : val.table_no,
                   order_type          : val.order_type,
                   order_date          : val.order_date,
                   orders_sale_price   : val.orders_sale_price,
                   payment_received    : val.payment_received,
                   order_status        : val.order_status,
                   first_name          : val.first_name,
                   last_name           : val.last_name,
                   address             : val.address,
                   product             : temData
               });
           }
           /*-- delete filter index --*/
           data = $.grep(data, function (item) {
               return item.order_id !== val.order_id;
           });
        });
        return arrangeData;
    }
});
/*
 * phone and indoor order controller
 */
app.controller('orderController', function ($scope, commonFunction, requestService, $uibModal, sharedProperties, $httpParamSerializerJQLike) {
    $scope.total = 0.00;
    $scope.grandTotal = 0.00;
    $scope.selectedProduct = [];
    var isFocus = true;
    var edit_item = null;
    /*
     * get customer data
     */
    $scope.usersList = function () {
        //active loader
        commonFunction.ActiveLoader();
        /*
         * call get request method from service
         */
        requestService.httpGetAjax(JS_Base_Path+'customer/getCustomers', function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.customersNo = [];
                $scope.customers = responseData.customers_info;
                angular.forEach($scope.customers, function (value, key) {
                    this.push(value.mobile)
                }, $scope.customersNo);
            }
            else
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            //stop loader
            commonFunction.StopLoader();
        });
    };
    /*-- call user function --*/
    $scope.usersList();
    /*
    * save phone order
    */
    $scope.savePhoneOrder = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $httpParamSerializerJQLike({
            'customer_no'   : $scope.userNo,
            'customer_name' : $scope.customerName,
            'address'       : $scope.customerAddress,
            'changed_info'  : $scope.changed_info,
            'total_discount': $scope.total_discount,
            'total_price'   : $scope.grandTotal,
            'data'          : $scope.selectedProduct
        }) , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    commonFunction.Toast(responseData.message, 'success');
                    $scope.usersList();
                    $('#savePhoneOrder')[0].reset();
                    $scope.grandTotal = 0;
                    $scope.selectedProduct = [];
                    resetValue();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                commonFunction.Toast(responseData[0], 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    };
    /*
     * save indoor order
     */
    $scope.saveIndoorOrder = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $httpParamSerializerJQLike({
            'table_no'      : $scope.tableNo,
            'total_discount': $scope.total_discount,
            'total_price'   : $scope.grandTotal,
            'data'          : $scope.selectedProduct
        }) , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveIndoorOrder')[0].reset();
                    $scope.grandTotal = 0;
                    $scope.selectedProduct = [];
                    resetValue();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                commonFunction.Toast(responseData[0], 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    };
    /*
     * selected user
     */
    $scope.selectUser = function (selected) {
        $scope.customer = $scope.customers.filter(function (val) {
            return val.mobile === selected
        });
        $scope.customerName = $scope.customer[0].first_name+' '+$scope.customer[0].last_name;
        $scope.customerAddress = $scope.customer[0].address;

    };
    /*
     * Product Model
     */
    $scope.productModel = function () {
        if(isFocus) {
            var modelInstance = null;
            commonFunction.showModal($uibModal, 'lg', '/dashboard/templates/order/product-list.html', 'productListController', function (instance) {
                isFocus = false;
                modelInstance = instance
            });
            modelInstance.result.finally(function () {
                if(sharedProperties.getValue('selectedItem')) {
                    var current_item = sharedProperties.getValue('selectedItem');
                    setValue(current_item);
                    $('#quantity').focus();
                    $scope.total = calculateTotal($scope.quantity, $scope.sale_price);
                }
            });
            modelInstance.result.catch(function () {
                $('#quantity').focus();
                $scope.focusToggle();
            })
        }
    };
    /*
     * Focus Toggle
     */
    $scope.focusToggle = function () {
        isFocus = true;
    };
    /*
    * update quantity
    */
    $scope.updateQuantity = function (quantity) {
        if(!quantity || quantity == 0)
            return true;
        if(sharedProperties.getValue('selectedItem')){
            if(sharedProperties.getValue('selectedItem').quantity){
                if(quantity >  sharedProperties.getValue('selectedItem').quantity) {
                    commonFunction.Toast('Please Enter valid Quantity', 'danger');
                    return true;
                }
            }
            $scope.total = calculateTotal(quantity, $scope.sale_price, $scope.discount);
        }
    };
    /*
     * update quantity
     */
    $scope.updateDiscount = function (discount) {
        if(sharedProperties.getValue('selectedItem') && discount > 0){
            if(discount >  sharedProperties.getValue('selectedItem').product.discount) {
                commonFunction.Toast('Please Enter valid Discount', 'danger');
                return true;
            }
        }
        $scope.total = calculateTotal($scope.quantity, $scope.sale_price, discount);
    };
    /*
     * update quantity
     */
    $scope.updatePrice = function (price) {
        if(!price || price == 0) {
            commonFunction.Toast('Please Enter valid price', 'danger');
            return true;
        }
        if($scope.selectedProduct){
            $scope.total = calculateTotal($scope.quantity, price, $scope.discount);
        }
    };
    /*
    * Add To List
    */
    $scope.addToList = function () {
        if(edit_item){
            var index = $scope.selectedProduct.indexOf(edit_item);
            setList(index);
            resetValue();
        }
        else if(sharedProperties.getValue('selectedItem')){
            $scope.selectedProduct.push(sharedProperties.getValue('selectedItem'));
            var last_index = $scope.selectedProduct.length - 1;
            setList(last_index);
        }
        resetValue();
    };
    /*
     * Reset Fields
     */
    $scope.resetFields = function () {
        resetValue();
    };
    /*
     * Edit select item from list
     */
    $scope.updateItem = function (item) {
        setValue(item);
        edit_item = item;
        isFocus = false;
        sharedProperties.setValue('selectedItem', item);
        $scope.total = calculateTotal(item.product.new_quantity, item.product.sale_price,item.product.discount);
        $scope.grandTotal = calculateGrandTotal();
    };
    /*
    * Delete select item from list
    */
    $scope.deleteItem = function (item) {
        var index = $scope.selectedProduct.indexOf(item);
        $scope.selectedProduct.splice(index, 1);
        $scope.grandTotal = calculateGrandTotal();
    };
    /*
     * Total discount
     */
    $scope.totalDiscount = function (discount) {
        $scope.grandTotal = calculateDiscount(discount, calculateGrandTotal());
    };
    /*
    * Reset Form
    */
    $scope.resetForm = function () {
        $('#savePhoneOrder')[0].reset();
        $scope.grandTotal = 0;
        $scope.selectedProduct = [];
        resetValue();
    };
    /*
     * calculate discount
     */
    var calculateDiscount = function (discount, price) {
        var data = commonFunction.calculateDiscount(discount, price);
        if(data)
            return data.price;
        else
            return price;
    };
    /*
     * calculate price
     */
    var calculatePrice = function (quantity, price) {
        var data = commonFunction.calculatePrice(quantity, price);
        if(data)
            return data;
    };
    /*
     * calculate Total
     */
    var calculateTotal = function (quantity, price, discount) {
        var total = calculatePrice(quantity, price);
        return calculateDiscount(discount, total);
    };
    /*
     * calculate Grand Total
     */
    var calculateGrandTotal = function () {
        var price = 0;
        if($scope.selectedProduct){
            angular.forEach($scope.selectedProduct, function (val) {
                price += calculateTotal(val.product.new_quantity, val.product.sale_price, val.product.discount);
            })
        }
        return price;
    };
    /*
     * set form value
     */
    var setValue = function (product) {
        $scope.item_name = product.product.name;
        $scope.quantity = product.product.new_quantity?product.product.new_quantity:1;
        $scope.discount = product.product.discount;
        $scope.sale_price = product.product.sale_price;
    };
    /*
     * Organize list
     */
    var setList = function (item_index) {
        $scope.selectedProduct[item_index].product.name = $scope.item_name;
        $scope.selectedProduct[item_index].product.new_quantity = $scope.quantity;
        $scope.selectedProduct[item_index].product.discount = $scope.discount;
        $scope.selectedProduct[item_index].product.sale_price = $scope.sale_price;
        $scope.grandTotal = calculateGrandTotal();
    };
    /*
     * Reset form value
     */
    var resetValue = function () {
        $scope.item_name = null;
        $scope.quantity = null;
        $scope.discount = null;
        $scope.sale_price = null;
        $scope.total = 0;
        edit_item = null;
        sharedProperties.setValue('selectedItem',null);
        $scope.focusToggle();
    }
});
/*
 * customer list information
 */
app.controller('customerListController', function ($scope, commonFunction, requestService) {
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get customer data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'customer/getCustomers', function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.customers = responseData.customers_info;
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
});
/*
 * product list information
 */
app.controller('productListController', function ($scope, $uibModalInstance, commonFunction, requestService, sharedProperties) {
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get customer data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'order/list', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.allProducts = responseData.data;
            $scope.allProductsCopy = $scope.allProducts;
        }
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
    /*
    * custom filter
    */
    $scope.typeFilter = function () {
        var data = [];
        if($scope.type == "all")
            $scope.allProducts = $scope.allProductsCopy;
        else {
            angular.forEach($scope.allProductsCopy, function (val) {
                angular.forEach(val, function (innerVal, key) {
                    if (key == $scope.type)
                        this.push(val)
                }, data)
            });
            $scope.allProducts = data;
        }
    };
    /*
    * select product
    */
    $scope.selectProduct = function (item) {
        if(item.product)
            sharedProperties.setValue('selectedItem', item);
        else {
            var data = {'product':item};
            sharedProperties.setValue('selectedItem', data);
        }
        $uibModalInstance.close();
    }
});