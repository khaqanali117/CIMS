/***************************************************************************************
 * Inventory Controller
 ****************************************************************************************/
/*
 Controller for Inventory list
 */
app.controller('inventoryList',function($scope, $http, $uibModal, commonFunction, Dialog, requestService){
    var stockId = null;
    $scope.index = null;
    $scope.category = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get inventory data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/inventory/list', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.inventoryItems = responseData.data;
            $scope.categories = responseData.categories;
            $scope.productCategories = responseData.categories;
            $scope.companies = responseData.companies;
            $scope.suppliers = responseData.suppliers;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    /*
    * Category filter
    */
    $scope.categoryFilter = function(item){
        if($scope.category != null) {
            if($scope.category == "all")
                return item;
            return (item.product.product_type == parseInt($scope.category))
        }
        else {
            return item;
        }
    };
    /*
     * Date filter
     */
    $scope.dateFilter = function(item){
        if($('#from_date').val() != '' && $('#to_date').val() != '') {
            return (item.expiry >= $('#from_date').val() && item.expiry <= $('#to_date').val())
        }
        else {
            return item;
        }
    };
    /*
    * load new inventory model
    */
    $scope.inventoryModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/inventory/inventory-item.html', 'newInventoryController', false);
    };
    /*
     * Edit inventory item information
     */
    $scope.updateInventory = function (item) {
        $scope.index = $scope.inventoryItems.indexOf(item);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/inventory/inventory-update.html', 'updateInventoryController', false);
    };
    /*
     * Delete inventory item information
     */
    $scope.deleteInventory = function (stock_id, item) {
        stockId = stock_id;
        $scope.index = $scope.inventoryItems.indexOf(item);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You want to delete this', deleteRequest);
    };

    /*
     * Delete inventory item request
     */
    var deleteRequest = function() {
        //active loader
         commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/inventory/delete/'+stockId, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.inventoryItems.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});

/*
 * save new inventory information
 */
app.controller('newInventoryController', function ($scope, commonFunction, requestService) {

    $scope.productCategories = $scope.$$prevSibling.productCategories;
    $scope.companies = $scope.$$prevSibling.companies;
    $scope.suppliers = $scope.$$prevSibling.suppliers;

    $scope.saveInventory = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.inventoryItems.push(responseData.data);
                    $('#saveInventory')[0].reset();
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});

/*
 * update inventory information
 */
app.controller('updateInventoryController', function ($scope, commonFunction, requestService) {
    /*
     * Get previous data
     */
    $scope.inventoryItems = angular.copy($scope.$$prevSibling.inventoryItems);
    $scope.inventoryItem_info = $scope.inventoryItems[$scope.$$prevSibling.index];

    $scope.productCategories = $scope.$$prevSibling.productCategories;
    $scope.companies = $scope.$$prevSibling.companies;
    $scope.suppliers = $scope.$$prevSibling.suppliers;


    $scope.updateCustomer = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.customers[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
