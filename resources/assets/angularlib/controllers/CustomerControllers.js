/***************************************************************************************
 * Customer Controller
 ****************************************************************************************/
/*
 Controller for customer page
 */
app.controller('customerList',function($scope,$http, $uibModal, commonFunction, Dialog, requestService){
    var customer_id = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get customer data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/customer/getCustomers', function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.customers = responseData.customers_info;
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
    //new customer model
    $scope.customerModal = function () {
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/customer/new-customer.html', 'newCustomerController', false);
    };
    //update customer model
    $scope.updateCustomer = function (customer) {
        $scope.index = $scope.customers.indexOf(customer);
        commonFunction.showModal($uibModal,'lg', '/dashboard/templates/customer/customer-update.html', 'updateCustomerController', false);
    };
    /*
     * Delete customer information
     */
    $scope.deleteCustomer = function (id, customer) {
        customer_id = id;
        $scope.index = $scope.customers.indexOf(customer);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You Want To Delete This User', deleteCustomerRequest);
    };
    /*
     * Delete customer request
     */
    var deleteCustomerRequest = function() {
        //active loader
        commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'/customer/deleteCustomer/'+customer_id, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.customers.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * save new customer information
 */
app.controller('newCustomerController', function ($scope, commonFunction, requestService) {

    $scope.saveCustomer = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.customers.push(responseData.data);
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveCustomer')[0].reset();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update customer information
 */
app.controller('updateCustomerController', function ($scope, commonFunction, requestService) {
    /*
     * Get previous data
     */
    $scope.customers = angular.copy($scope.$$prevSibling.customers);
    $scope.customer_info = $scope.customers[$scope.$$prevSibling.index];

    $scope.updateCustomer = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path+url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    $scope.$$prevSibling.customers[$scope.$$prevSibling.index] = responseData.data;
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});