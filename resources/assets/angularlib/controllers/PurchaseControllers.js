/***************************************************************************************
 * Purchase controller
 ****************************************************************************************/
/*
 Controller for supplier page
 */
app.controller('purchaseList',function($scope,$http, commonFunction, Dialog, requestService){
    //active loader
    commonFunction.ActiveLoader();
    /*
     * Add new purchase
     * call get request method from service
     */
    // requestURL, requestData, callBack
    /*requestService.httpPostAjax(JS_Base_Path+'/purchase/addNew', requestData, function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.products = responseData.products_info;
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });*/

    /*
     * get supplier data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/purchase/getProducts', function (responseData, responseType) {
        if(responseData.isResponse)
            $scope.products = responseData.products_info;
        else
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        //stop loader
        commonFunction.StopLoader();
    });
});