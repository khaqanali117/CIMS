/***************************************************************************************
 * Deals Controller
 ****************************************************************************************/
/*
 Controller for Menu list
 */
app.controller('dealsController',function($scope, $uibModal, commonFunction, Dialog, requestService){
    var dealId = null;
    $scope.index = null;
    //active loader
    commonFunction.ActiveLoader();
    /*
     * get deals data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/deals/list', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.dealsList = responseData.data;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    /*
     * Delete deal item information
     */
    $scope.deleteDeal = function (deal_id, item) {
        dealId = deal_id;
        $scope.index = $scope.dealsList.indexOf(item);
        //Delete Dialog
        Dialog.Warning("Are you sure?",'You want to delete this', deleteDealRequest);
    };

    /*
     * Delete deal item request
     */
    var deleteDealRequest = function() {
        //active loader
         commonFunction.ActiveLoader();
        //call get request method from service
        requestService.httpGetAjax(JS_Base_Path+'deals/delete/'+dealId, function (responseData, responseType) {
            if(responseData.isResponse) {
                $scope.dealsList.splice($scope.index, 1);
                commonFunction.Toast(responseData.message, 'success');
            }else {
                commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
    /*
     * deal component
     */
    angular.element(document).ready(function () {
        //add new deals component row
        $(document).on('click', '.add-deal-component', function () {
            var parents_el = $(this).parents('.deal-component');
            var clone_html = parents_el.clone();
            clone_html.find('.quantity').val('');
            clone_html.find('.dropdown-toggle').remove();
            clone_html.find('.dropdown-menu').remove();
            parents_el.after(clone_html);
            commonFunction.initializeCoreLib();
            $('.deal-component').not(':first').find('.remove-deal-component').removeClass('hide');
        });

        //add new schedule row
        $(document).on('click', '.add-deal-schedule', function () {
            var parents_el = $(this).parents('.deal-schedule');
            var clone_html = parents_el.clone();
            clone_html.find('input').val('');
            parents_el.after(clone_html);
            commonFunction.initializeCoreLib();
            $('.deal-schedule').not(':first').find('.remove-deal-schedule').removeClass('hide');
        });
        //remove component row
        $(document).on('click', '.remove-deal-component', function () {
            $(this).parents('.deal-component').remove();
        });
        //remove schedule row
        $(document).on('click', '.remove-deal-schedule', function () {
            $(this).parents('.deal-schedule').remove();
        });
    });
});
/*
 * save new deal information
 */
app.controller('newDealController', function ($scope, $uibModal, commonFunction, requestService) {
    /*
     * get product and menu data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'/deals/create', function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.allItems = responseData.data;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });

    $scope.saveDeal = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    commonFunction.Toast(responseData.message, 'success');
                    $('#saveDeal')[0].reset();
                    $('.deal-component').not(':first').remove();
                    $('.deal-schedule').not(':first').remove();
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }
});
/*
 * update deal information
 */
app.controller('updateDealController', function ($scope, $routeParams, $filter, $uibModal, commonFunction, requestService) {
    var id = $routeParams.item;
    /*
     * get product and menu data
     * call get request method from service
     */
    requestService.httpGetAjax(JS_Base_Path+'deals/update/'+id, function (responseData, responseType) {
        if(responseData.isResponse) {
            $scope.deal_info = responseData.data;
            $scope.allItems = responseData.items;
        }else {
            commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
        }
        //stop loader
        commonFunction.StopLoader();
    });
    $scope.selectComponent = function (item,comp) {
        var item_col = item.product_id?'product_id':'menu_id';
        var comp_col = comp.product_id?'product_id':'menu_id';
        var item_id = item.product_id?item.product_id:item.menu_id;
        var comp_id = comp.product_id?comp.product_id:comp.menu_id;
        if(item_col == comp_col && item_id == comp_id) {
            $scope.prev_component = item_id+'-'+item_col;
            return true;
        }
        return false;
    };

    $scope.updateDeal = function (event) {
        var url = event.target.attributes['data-url'].value;
        //active loader
        commonFunction.ActiveLoader();
        commonFunction.RemoveMessage();
        //call post request method from service
        requestService.httpPostAjax(JS_Base_Path + url, $(event.target).serialize() , function (responseData, responseType) {
            if(responseType) {
                if (responseData.isResponse) {
                    commonFunction.Toast(responseData.message, 'success');
                }
                else
                    commonFunction.Toast('Oops!Something Went Wrong..', 'danger');
            }else {
                $('#messages').html(commonFunction.Error_function(responseData))
            }
            //stop loader
            commonFunction.StopLoader();
        });
    }

    /*
     * deal component
     */
    angular.element(document).ready(function () {
        //add new deals component row
        $(document).on('click', '.update-deal-component', function () {
            var parents_el = $(this).parents('.deal-component');
            var clone_html = parents_el.clone();
            clone_html.find('.quantity').val('');
            clone_html.find('.dropdown-toggle').remove();
            clone_html.find('.dropdown-menu').remove();
            parents_el.after(clone_html);
            commonFunction.initializeCoreLib();
            $('.deal-component').not(':first').find('.remove-update-deal-component').removeClass('hide');
        });

        //add new schedule row
        $(document).on('click', '.update-deal-schedule', function () {
            var parents_el = $(this).parents('.deal-schedule');
            var clone_html = parents_el.clone();
            clone_html.find('input').val('');
            parents_el.after(clone_html);
            commonFunction.initializeCoreLib();
            $('.deal-schedule').not(':first').find('.remove-update-deal-schedule').removeClass('hide');
        });
        //remove component row
        $(document).on('click', '.remove-update-deal-component', function () {
            $(this).parents('.deal-component').remove();
        });
        //remove schedule row
        $(document).on('click', '.remove-update-deal-schedule', function () {
            $(this).parents('.deal-schedule').remove();
        });
    });
});

