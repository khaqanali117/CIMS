<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/**
 * Routes For Auth
 */
Route::group(['prefix' => 'auth','middleware' => 'guest'], function () {
    /**
     * User Login route
     */
    Route::get('/login', [
        'as' => 'getLogin',
        'uses' => 'UserLoginController@getLogin'
    ]);
    /**
     * User Login route
     */
    Route::post('/login', [
        'as' => 'postLogin',
        'uses' => 'UserLoginController@postLogin'
    ]);

});
Route::group(['middleware' => 'auth'],function () {
    /**
     * Admin dashboard route
     */
    Route::get('/', [
        'as' => 'adminIndex',
        'uses' => 'AdminController@index'
    ]);
    /**
     * Routes For user
     */
    Route::group(['prefix' => 'user'], function () {

        Route::get('getUsers/', [
            'as' => 'getUsers',
            'uses' => 'AdminController@getUsers'
        ]);
        /**
         * user add route
         */
        Route::get('createUser', [
            'as' => 'getCreateUser',
            'uses' => 'AdminController@getCreateUser'
        ]);
        /**
         * user save route
         */
        Route::post('createUser', [
            'as' => 'postCreateUser',
            'uses' => 'AdminController@postCreateUser'
        ]);
        /**
         * user update route(not used)
         */
        Route::get('updateUser/{user_id}', [
            'as' => 'getUpdateUser',
            'uses' => 'AdminController@getUpdateUser'
        ]);
        /**
         * user update route
         */
        Route::post('updateUser', [
            'as' => 'postUpdateUser',
            'uses' => 'AdminController@postUpdateUser'
        ]);
        /**
         * User delete route
         */
        Route::get('deleteUser/{user_id}', [
            'as' => 'getDeleteUser',
            'uses' => 'AdminController@getDeleteUser'
        ]);
        /**
         * User profile route
         */
        Route::get('userProfile', [
            'as' => 'getUserProfile',
            'uses' => 'UserLoginController@getUserProfile'
        ]);
        /**
         * User profile route
         */
        Route::post('userProfile', [
            'as' => 'postUserProfile',
            'uses' => 'UserLoginController@postUserProfile'
        ]);
    });
    /**
     * Routes For supplier
     */
    Route::group(['prefix' => 'purchase'], function () {

        Route::get('getProducts/', [
            'as' => 'getProducts',
            'uses' => 'PurchaseController@getProducts'
        ]);

        Route::post('getProducts/', [
            'as' => 'getProducts',
            'uses' => 'PurchaseController@getProducts'
        ]);

    });
    Route::group(['prefix' => 'supplier'], function () {

        Route::get('getSuppliers/', [
            'as' => 'getSuppliers',
            'uses' => 'SupplierController@getSuppliers'
        ]);
        /**
         * supplier add route
         */
        Route::post('createSuppliers', [
            'as' => 'postCreateSuppliers',
            'uses' => 'SupplierController@postCreateSuppliers'
        ]);
        /**
         * supplier update route(not used yet)
         */
        Route::get('updateSupplier/{supplier_id}', [
            'as' => 'getUpdateSupplier',
            'uses' => 'SupplierController@getUpdateSupplier'
        ]);
        /**
         * supplier update route
         */
        Route::post('updateSupplier', [
            'as' => 'postUpdateSupplier',
            'uses' => 'SupplierController@postUpdateSupplier'
        ]);
        /**
         * Supplier delete route
         */
        Route::get('deleteSupplier/{supplier_id}', [
            'as' => 'getDeleteSupplier',
            'uses' => 'SupplierController@getDeleteSupplier'
        ]);
    });
    /**
     * Routes For customer
     */
    Route::group(['prefix' => 'customer'], function () {

        Route::get('getCustomers/', [
            'as' => 'getCustomers',
            'uses' => 'CustomerController@getCustomers'
        ]);
        /**
         * customer add route
         */
        Route::post('createCustomer', [
            'as' => 'postCreateCustomer',
            'uses' => 'CustomerController@postCreateCustomer'
        ]);
        /**
         * customer update route(not used yet)
         */
        Route::get('updateCustomer/{customer_id}', [
            'as' => 'getUpdateCustomer',
            'uses' => 'CustomerController@getUpdateCustomer'
        ]);
        /**
         * customer update route
         */
        Route::post('updateCustomer', [
            'as' => 'postUpdateCustomer',
            'uses' => 'CustomerController@postUpdateCustomer'
        ]);
        /**
         * Customer delete route
         */
        Route::get('deleteCustomer/{customer_id}', [
            'as' => 'getDeleteCustomer',
            'uses' => 'CustomerController@getDeleteCustomer'
        ]);
    });
    /**
     * Routes For inventory
     */
    Route::group(['prefix' => 'inventory'], function () {
        /**
         * Inventory item list route
         */
        Route::get('list', [
            'as' => 'inventoryList',
            'uses' => 'InventoryController@inventoryList'
        ]);
        /**
         * Inventory add item route(not yet used)
         */
        Route::get('create', [
            'as' => 'getCreate',
            'uses' => 'InventoryController@getCreate'
        ]);
        /**
         * Inventory save item route
         */
        Route::post('create', [
            'as' => 'postCreate',
            'uses' => 'InventoryController@postCreate'
        ]);
        /**
         * Inventory update item route(not yet used)
         */
        Route::get('update/{stock_id}', [
            'as' => 'getUpdate',
            'uses' => 'InventoryController@getUpdate'
        ]);
        /**
         * Inventory update item route
         */
        Route::post('update', [
            'as' => 'postUpdate',
            'uses' => 'InventoryController@postUpdate'
        ]);
        /**
         * Inventory delete item route
         */
        Route::get('delete/{stock_id}', [
            'as' => 'getDelete',
            'uses' => 'InventoryController@getDelete'
        ]);
    });
    /**
     * Routes For menu
     */
    Route::group(['prefix' => 'menu'], function () {
        /**
         * Menu list route
         */
        Route::get('list', [
            'as' => 'menuList',
            'uses' => 'MenuController@menuList'
        ]);
        /**
         * Menu add item route
         */
        Route::get('create', [
            'as' => 'getCreateMenu',
            'uses' => 'MenuController@getCreateMenu'
        ]);
        /**
         * Menu save route
         */
        Route::post('create', [
            'as' => 'postCreateMenu',
            'uses' => 'MenuController@postCreateMenu'
        ]);
        /**
         * Menu update route(not yet used)
         */
        Route::get('update/{menu_id}', [
            'as' => 'getUpdateMenu',
            'uses' => 'MenuController@getUpdate'
        ]);
        /**
         * Menu update route
         */
        Route::post('update', [
            'as' => 'postUpdateMenu',
            'uses' => 'MenuController@postUpdate'
        ]);
        /**
         * Menu delete route
         */
        Route::get('delete/{menu_id}', [
            'as' => 'getDeleteMenu',
            'uses' => 'MenuController@getDelete'
        ]);
    });
    /**
     * Routes For deals
     */
    Route::group(['prefix' => 'deals'], function () {
        /**
         * Deals list route
         */
        Route::get('list', [
            'as' => 'dealsList',
            'uses' => 'DealsController@dealsList'
        ]);
        /**
         * Deal add route
         */
        Route::get('create', [
            'as' => 'getCreateDeals',
            'uses' => 'DealsController@productMenu'
        ]);
        /**
         * Deal save route
         */
        Route::post('create', [
            'as' => 'postCreateDeal',
            'uses' => 'DealsController@postCreateDeal'
        ]);
        /**
         * Deal update route
         */
        Route::get('update/{deal_id}', [
            'as' => 'getUpdateDeal',
            'uses' => 'DealsController@getUpdateDeal'
        ]);
        /**
         * Menu update route
         */
        Route::post('update', [
            'as' => 'postUpdateDeal',
            'uses' => 'DealsController@postUpdateDeal'
        ]);
        /**
         * Deal delete route
         */
        Route::get('delete/{deal_id}', [
            'as' => 'getDeleteDeal',
            'uses' => 'DealsController@getDeleteDeal'
        ]);
    });
    /**
     * Routes For category
     */
    Route::group(['prefix' => 'category'], function () {
        /**
         * Category list route
         */
        Route::get('list', [
            'as' => 'categoryList',
            'uses' => 'CategoryController@categoryList'
        ]);
        /**
         * Category save route
         */
        Route::post('create', [
            'as' => 'postCreateCategory',
            'uses' => 'CategoryController@postCreate'
        ]);
        /**
         * Category update route(not yet used)
         */
        Route::get('update/{category_id}', [
            'as' => 'getUpdateCategory',
            'uses' => 'CategoryController@getUpdate'
        ]);
        /**
         * Category update route
         */
        Route::post('update', [
            'as' => 'postUpdateCategory',
            'uses' => 'CategoryController@postUpdate'
        ]);
        /**
         * Category delete route
         */
        Route::get('delete/{category_id}', [
            'as' => 'getDeleteCategory',
            'uses' => 'CategoryController@getDelete'
        ]);
    });
    /**
     * Routes For order
     */
    Route::group(['prefix' => 'order'], function (){
        /**
         * Order list route
         */
        Route::get('orderList', [
            'as'    => 'orderList',
            'uses'  => 'OrderController@orderList'
        ]);
        /**
         * Products list route
         */
        Route::get('list', [
            'as'    => 'productList',
            'uses'  => 'OrderController@productList'
        ]);
        /**
         * Phone Order create route
         */
        Route::post('phoneCreate', [
            'as'    => 'postPhoneOrderCreate',
            'uses'  => 'OrderController@postPhoneOrderCreate'
        ]);
        /**
         * Indoor Order create route
         */
        Route::post('indoorCreate', [
            'as'    => 'postIndoorOrderCreate',
            'uses'  => 'OrderController@postIndoorOrderCreate'
        ]);
    });
});

/**
 * Logout route
 */
Route::get('getLogout', [
    'as' => 'getLogout',
    'uses' => 'UserLoginController@getLogout'
]);
