const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
// assets directory path
var path = {
    components: './resources/assets/vendors/bower_components/',
    vendors: './resources/assets/vendors/',
    angularLib: './resources/assets/angularlib/',
};

elixir(function(mix) {
    mix.styles([
        path.components+'fullcalendar/dist/fullcalendar.min.css',
        path.components+'animate.css/animate.min.css',
        path.components+'sweetalert/dist/sweetalert.css',
        path.components+'material-design-iconic-font/dist/css/material-design-iconic-font.min.css',
        path.components+'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
        path.components+'bootstrap-select/dist/css/bootstrap-select.css',
        path.components+'chosen/chosen.css',
        path.vendors+'summernote/dist/summernote.css',
        path.components+'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'app_1.min.css',
        'app_2.min.css',
        'autocomplete.css',
        'custom.css'
    ],'public/dashboard/css/all.css');
});
elixir(function(mix) {
    mix.scripts([
        path.components+'jquery/dist/jquery.min.js',
        path.components+'bootstrap/dist/js/bootstrap.min.js',
        path.components+'Waves/dist/waves.min.js',
        path.components+'flot/jquery.flot.js',
        path.components+'flot/jquery.flot.resize.js',
        path.components+'flot.curvedlines/curvedLines.js',
        path.vendors+'sparklines/jquery.sparkline.min.js',
        path.components+'jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
        path.components+'moment/min/moment.min.js',
        path.components+'fullcalendar/dist/fullcalendar.min.js',
        path.components+'simpleWeather/jquery.simpleWeather.min.js',
        path.vendors+'bootstrap-growl/bootstrap-growl.min.js',
        path.components+'sweetalert/dist/sweetalert.min.js',
        path.components+'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
        path.components+'bootstrap-select/dist/js/bootstrap-select.js',
        path.components+'chosen/chosen.jquery.js',
        path.components+'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'app.min.js',
        path.angularLib + 'angular.1.5.7.js',
        path.angularLib + 'angular-route.js',
        path.angularLib + 'angular-animate.1.5.8.js',
        path.angularLib + 'angular-sanitize.1.5.8.js',
        path.angularLib + 'ui-bootstrap-tpls-2.2.0.js',
        path.angularLib + 'autocomplete.js',
        path.angularLib + 'app.js',
        path.angularLib + 'routes.js',
        path.angularLib + 'service.js',
        path.angularLib + 'factory.js',
        path.angularLib + 'directive.js',
        path.angularLib + 'controllers/DashboardControllers.js',
        path.angularLib + 'controllers/UserControllers.js',
        path.angularLib + 'controllers/SupplierControllers.js',
        path.angularLib + 'controllers/CustomerControllers.js',
        path.angularLib + 'controllers/UserProfileControllers.js',
        path.angularLib + 'controllers/InventoryControllers.js',
        path.angularLib + 'controllers/CategoryControllers.js',
        path.angularLib + 'controllers/MenuControllers.js',
        path.angularLib + 'controllers/DealsControllers.js',
        path.angularLib + 'controllers/OrderControllers.js',
        path.angularLib + 'controllers/PurchaseControllers.js'
    ],'public/dashboard/js/all.js');
});

elixir(function(mix) {
    mix.version(['dashboard/css/all.css', 'dashboard/js/all.js']);
});
elixir(function(mix) {
    mix.browserSync({
        proxy: 'cims.app'
    });
});
