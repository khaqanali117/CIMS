<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'first_name'    => 'Tester',
                'last_name'     => 'Tester',
                'address'       => 'Tester',
                'city'          => 'Tester',
                'cell_phone'    => '03347151183',
                'email'         => 'admin@admin.com',
                'password'      => bcrypt('admin'),
                'created_by'    => 1,
            ],[
                'first_name'    => 'Tester',
                'last_name'     => 'Tester',
                'address'       => 'Tester',
                'city'          => 'Tester',
                'cell_phone'    => '123456',
                'email'         => 'admin1@admin.com',
                'password'      => bcrypt('admin'),
                'created_by'    => 1,
            ]
        ];
        DB::table('users')->insert($data);
    }
}
