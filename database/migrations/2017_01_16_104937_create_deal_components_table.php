<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealComponentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deal_components', function(Blueprint $table)
		{
			$table->increments('deal_component_id');
			$table->integer('product_id')->nullable()->index('product_id');
			$table->integer('deal_id')->index('deal_id');
			$table->integer('menu_id')->nullable()->index('item_id');
			$table->boolean('item_type')->nullable();
            $table->string('quantity')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deal_components');
	}

}
