<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('address')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->integer('zip')->nullable();
			$table->string('home_phone')->nullable();
			$table->string('cell_phone')->nullable();
			$table->string('work_phone')->nullable();
			$table->string('email')->nullable()->unique();
			$table->string('email2')->nullable();
			$table->string('password')->nullable();
			$table->integer('created_by')->unsigned()->nullable();
			$table->dateTime('last_login')->nullable();
			$table->boolean('status')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('profile_image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
