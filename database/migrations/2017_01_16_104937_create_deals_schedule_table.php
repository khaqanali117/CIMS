<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealsScheduleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deals_schedule', function(Blueprint $table)
		{
			$table->increments('deal_schedule_id');
			$table->integer('deal_id')->index('deal_id');
			$table->time('deal_start_time')->nullable();
			$table->time('deal_end_time')->nullable();
			$table->date('deal_start_date')->nullable();
			$table->date('deal_end_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deals_schedule');
	}

}
