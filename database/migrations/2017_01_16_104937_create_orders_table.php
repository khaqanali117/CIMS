<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('order_id');
			$table->integer('customer_id');
            $table->string('table_no', 50)->nullable();
            $table->integer('sale_price')->nullable();
            $table->integer('order_discount')->nullable();
            $table->integer('payment_received')->nullable();
            $table->dateTime('order_date')->nullable();
            $table->integer('created_by');
            $table->boolean('order_type')->default(0)->comment('1 means "phone" order 0 menas "indoor" order');
            $table->boolean('order_status')->nullable()->default(0)->comment('0 means pending 1 means confirm 2 means cancel');
            $table->timestamps();
            $table->softDeletes();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
