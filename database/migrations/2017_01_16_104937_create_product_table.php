<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('product_id');
			$table->string('product_name', 100);
			$table->string('product_code', 150)->nullable();
			$table->string('sku', 100)->nullable();
			$table->string('unit', 20)->nullable();
			$table->tinyInteger('stock_level')->nullable();
			$table->integer('min_stock_level')->nullable();
			$table->decimal('cost_price', 10, 0);
			$table->decimal('sale_price', 10, 0);
			$table->string('location', 20)->nullable();
			$table->tinyInteger('product_type')->nullable();
			$table->string('description')->nullable();
			$table->decimal('discount', 10, 0)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}
