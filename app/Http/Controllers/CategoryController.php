<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoryList()
    {
        $categories = ProductCategory::get();
        return response()->json([
            'isResponse' => true,
            'data' => $categories
        ]);
    }

    /**
     * @param CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(CategoryRequest $request)
    {
        $category = ProductCategory::create($request->all());
        return response()->json([
            'isResponse' => true,
            'data' => $category,
            'message' => 'New Category Saved Successfully'
        ]);
    }

    /**
     * @param $category_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdate($category_id)
    {
        $category = ProductCategory::findOrFail($category_id);

        $renderHtml = view('dashboard/category/category-update', compact('category'))->render();

        return response()->json([
            'isResponse' => true,
            'data' => $renderHtml
        ]);
    }

    /**
     * @param CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(CategoryRequest $request)
    {
        $category = ProductCategory::findOrFail($request->category_id);
        $category->category_name = $request->category_name;
        $category->description = $request->description;
        $category->save();

        return response()->json([
            'isResponse' => true,
            'data' => $category,
            'message' => 'Category Information Update Successfully'
        ]);
    }

    /**
     * @param $category_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDelete($category_id)
    {
        ProductCategory::findOrFail($category_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'Category Information Deleted Successfully'
        ]);
    }
}
