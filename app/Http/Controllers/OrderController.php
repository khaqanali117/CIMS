<?php

namespace App\Http\Controllers;


use App\Http\HelperModules\HelperModules;
use App\Http\HelperModules\ProductModule;
use App\Http\Requests\IndoorOrderRequest;
use App\Http\Requests\PhoneOrderRequest;
use App\Models\Customer;
use App\Models\OrderItems;
use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderList()
    {
        $select = [
            'orders.order_id',
            'orders.table_no',
            'orders.order_type',
            'orders.order_date',
            'orders.sale_price as orders_sale_price',
            'orders.payment_received',
            'orders.order_status',
            'orders.order_discount as total_discount',
            'customers.mobile',
            'customers.first_name',
            'customers.last_name',
            'customers.address',
            'order_items.item_id',
            'order_items.item_type',
            'order_items.quantity',
            'order_items.sale_price',
            'order_items.item_discount',
            'menu.title',
            'deals.deal_name'
        ];
        $order = new Orders();
        $orders_list = $order->ordersList($select)
            ->whereNull('orders.deleted_at')
            ->whereNull('order_items.deleted_at')
            ->orderBy('orders.order_date', 'desc')
            ->get();

        if(count($orders_list) > 0)
            return HelperModules::responseJson(true, '200', false, false, $orders_list);

        return HelperModules::responseJson(true, '200', false, Lang::get('generalMessages.success.not_found', ['name' => 'Record']));
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productList()
    {
        $productModuleObj = new ProductModule();
        $allItems = $productModuleObj->productMenuDeal();

        return HelperModules::responseJson(true, '200', false, false, $allItems);
    }

    /**
     * @param PhoneOrderRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postPhoneOrderCreate(PhoneOrderRequest $request)
    {
        Log::info($request->all());
        $order_items = [];
        /*First check and create customer information*/
        $customer_info = Customer::where('mobile', $request->customer_no)->first();
        if($customer_info){
            if($request->has('changed_info')) {
                $customer_info->mobile = $request->customer_no;
                $customer_info->address = $request->address;
                $customer_info->save();
            }
        }
        else{
            $customer_info = Customer::create([
                'first_name'    => 'Phone',
                'last_name'     => 'Order',
                'address'       => $request->address,
                'mobile'        => $request->customer_no,
            ]);
        }

        /*save order info*/
        $order = Orders::create([
            'customer_id'       => $customer_info->customer_id,
            'sale_price'        => $request->total_price,
            'order_discount'    => $request->total_discount?$request->total_discount:0,
            'payment_received'  => $request->cash_received?$request->cash_received:0,
            'order_status'      => $request->cash_received?1:0,
            'order_type'        => 1,
            'order_date'        => Carbon::now(),
            'created_by'        => Auth::user()->id
        ]);

        /*get order items info*/
        foreach ($request->data as $item){
            if($item['product']['name'] && $item['product']['new_quantity'] && $item['product']['sale_price']){

                if(isset($item['product']['product_id'])) {
                    $item_id    = $item['stock_id'];
                    $item_type  = "product";
                }
                elseif (isset($item['product']['menu_id'])) {
                    $item_id    = $item['product']['menu_id'];
                    $item_type  = "menu";
                }
                else {
                    $item_id    = $item['product']['deal_id'];
                    $item_type  = "deal";
                }
                if(isset($item['product']['discount']))
                    $item_discount = $item['product']['discount'];
                else
                    $item_discount = 0;

                array_push($order_items,[
                    'order_id'      => $order->order_id,
                    'item_id'       => $item_id,
                    'item_type'     => $item_type,
                    'item_name'     => $item['product']['name'],
                    'sale_price'    => $item['product']['sale_price'],
                    'quantity'      => $item['product']['new_quantity'],
                    'item_discount' => $item_discount?$item_discount:0,
                ]);
            }
        }
        /*save order items info*/
        OrderItems::insert($order_items);

        return HelperModules::responseJson(true, '200', false, Lang::get('generalMessages.success.create', ['name' => 'Order']));
    }

    /**
     * @param IndoorOrderRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postIndoorOrderCreate(IndoorOrderRequest $request)
    {
        $order_items = [];
        /*save order info*/
        $order = Orders::create([
            'customer_id'       => 0,
            'sale_price'        => $request->total_price,
            'order_discount'    => $request->total_discount?$request->total_discount:0,
            'payment_received'  => $request->cash_received?$request->cash_received:0,
            'order_type'        => 0,
            'order_date'        => Carbon::now(),
            'table_no'          => $request->table_no?$request->table_no:'',
            'created_by'        => Auth::user()->id
        ]);

        /*get order items info*/
        foreach ($request->data as $item){
            if($item['product']['name'] && $item['product']['new_quantity'] && $item['product']['sale_price']){

                if(isset($item['product']['product_id'])) {
                    $item_id    = $item['stock_id'];
                    $item_type  = "product";
                }
                elseif (isset($item['product']['menu_id'])) {
                    $item_id    = $item['product']['menu_id'];
                    $item_type  = "menu";
                }
                else {
                    $item_id    = $item['product']['deal_id'];
                    $item_type  = "deal";
                }
                if(isset($item['product']['discount']))
                    $item_discount = $item['product']['discount'];
                else
                    $item_discount = 0;

                array_push($order_items,[
                    'order_id'      => $order->order_id,
                    'item_id'       => $item_id,
                    'item_type'     => $item_type,
                    'item_name'     => $item['product']['name'],
                    'sale_price'    => $item['product']['sale_price'],
                    'quantity'      => $item['product']['new_quantity'],
                    'item_discount' => $item_discount?$item_discount:0,
                ]);
            }
        }
        /*save order items info*/
        OrderItems::insert($order_items);

        return HelperModules::responseJson(true, '200', false, Lang::get('generalMessages.success.create', ['name' => 'Order']));
    }
}
