<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductCompany;
use App\Models\ProductSupplier;
use App\Models\Stock;
use App\Models\Supplier;
use App\Http\Requests\InventoryItemRequest;
use Illuminate\Support\Facades\Log;

class InventoryController extends Controller
{
    /**
     * InventoryController constructor.
     */
    public function __construct()
    {
        $this->productObj = new Product();
        $this->stockObj = new Stock();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function inventoryList()
    {
        //Get all categories
        $productCategories = ProductCategory::get();
        //Get all Companies
        $companies = Company::get();
        //Get all Supplier
        $suppliers = Supplier::get();
        $select = ['product_id','product_name','product_type','cost_price','sale_price','description','min_stock_level'];
        $products = $this->stockObj->product_info($select)->get();
        
        return response()->json([
            'isResponse' => true,
            'data' => $products,
            'categories' => $productCategories,
            'companies'  => $companies,
            'suppliers'  => $suppliers
        ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCreate()
    {
        //Get all categories
        $productCategories = ProductCategory::get();
        //Get all Companies
        $companies = Company::get();
        //Get all Supplier
        $suppliers = Supplier::get();

        return response()->json([
            'isResponse' => true,
            'productCategories' => $productCategories,
            'companies'         => $companies,
            'suppliers'         => $suppliers
        ]);
    }

    /**
     * @param InventoryItemRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreate(InventoryItemRequest $request)
    {

        $productInfo = Product::create($request->all());
        //inject product into request
        $request['product_id'] = $productInfo->product_id;
        //save stock info
        $stockInfo = Stock::create($request->all());
        //inject product into request
        $request['stock_id'] = $stockInfo->stock_id;
        //save supplier info
        ProductSupplier::create($request->all());
        //save company info
        ProductCompany::create($request->all());
        //get new inserted product
        $productInfo = $this->stockObj->product_info()->where('stock_id',$stockInfo->stock_id)->first();

        return response()->json([
            'isResponse' => true,
            'data' => $productInfo,
            'message' => 'New Inventory Item Saved Successfully'
        ]);
    }

    /**
     * @param $stock_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function getUpdate($stock_id)
    {
        $productInfo = $this->stockObj->product_info()->where('stock_id',$stock_id)->first();
        //Get all categories
        $productCategories = ProductCategory::get();
        //Get all Companies
        $companies = Company::get();
        //Get all Supplier
        $suppliers = Supplier::get();

        $renderHtml = view('dashboard/inventory/inventory-update', compact('productInfo','productCategories', 'companies', 'suppliers'))->render();

        return response()->json([
            'isResponse' => true,
            'data' => $renderHtml
        ]);
    }

    /**
     * @param InventoryItemRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(InventoryItemRequest $request)
    {
        //update stock info
        $stockInfo = Stock::findOrFail($request->stock_id);
        $stockInfo->batch_no = $request->batch_no;
        $stockInfo->quantity = $request->quantity;
        $stockInfo->purchase_date = $request->purchase_date;
        $stockInfo->expiry = $request->expiry;
        $stockInfo->save();

        //update product info
        $productInfo = Product::findOrFail($stockInfo->product_id);
        $productInfo->product_name = $request->product_name;
        $productInfo->product_type = $request->product_type;
        $productInfo->cost_price = $request->cost_price;
        $productInfo->sale_price = $request->sale_price;
        $productInfo->min_stock_level = $request->min_stock_level;
        $productInfo->save();

        //update supplier info
        ProductSupplier::where('stock_id',$request->stock_id)
            ->update(['supplier_id' => $request->supplier_id]);
        //update company info
        ProductCompany::where('stock_id',$request->stock_id)
            ->update(['company_id' => $request->company_id]);

        //get updated product
        $productInfo = $this->stockObj->product_info()->where('stock_id',$request->stock_id)->first();
        return response()->json([
            'isResponse' => true,
            'data' => $productInfo,
            'message' => 'Inventory Information Update Successfully'
        ]);
    }

    /**
     * @param $stock_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDelete($stock_id)
    {
        Stock::find($stock_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'Inventory Information Deleted Successfully'
        ]);
    }
}
