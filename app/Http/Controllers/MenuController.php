<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuComponent;
use App\Models\Product;
use App\Http\Requests\MenuRequest;

class MenuController extends Controller
{
    public function menuList()
    {
        //get menu list
        $menu_list = Menu::MenuList()->get();

        return response()->json([
            'isResponse' => true,
            'data' => $menu_list
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCreateMenu()
    {
        //get all products
        $products = Product::get();

        return response()->json([
           'isResponse' => true,
           'data' => $products
        ]);
    }

    /**
     * @param MenuRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateMenu(MenuRequest $request)
    {
        //add new menu
        $menu = Menu::create($request->all());
        for($i = 0; $i < count($request->product_id); $i++){
            if(!empty($request->product_id[$i] && !empty($request->quantity[$i]))){
                MenuComponent::create([
                    'menu_id'       => $menu->menu_id,
                    'product_id'    => $request->product_id[$i],
                    'quantity'      => $request->quantity[$i]
                ]);
            }
        }
        //get new inserted menu
        $menu_list = Menu::MenuList()->where('menu_id', $menu->menu_id)->first();
        return response()->json([
            'isResponse' => true,
            'data' => $menu_list,
            'message' => 'New Menu Added Successfully'
        ]);
    }

    /**
     * @param $menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdate($menu_id)
    {
        //get all products
        $products = Product::get();
        //get new inserted menu
        $menu_list = Menu::MenuList()->where('menu_id', $menu_id)->first();
        $renderHtml = view('dashboard/menu/menu-update', compact('products', 'menu_list'))->render();

        return response()->json([
            'isResponse' => true,
            'data' => $renderHtml
        ]);
    }

    /**
     * @param MenuRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(MenuRequest $request)
    {
        //update menu
        $menu = Menu::findOrFail($request->menu_id);
        $menu->title = $request->title;
        $menu->description = $request->description;
        $menu->save();
        //delete previous menu components
        MenuComponent::where('menu_id', $request->menu_id)->delete();
        //save new menu
        for($i = 0; $i < count($request->product_id); $i++){
            if(!empty($request->product_id[$i] && !empty($request->quantity[$i]))){
                MenuComponent::create([
                    'menu_id'       => $menu->menu_id,
                    'product_id'    => $request->product_id[$i],
                    'quantity'      => $request->quantity[$i]
                ]);
            }
        }
        //get updated menu
        $menu_list = Menu::MenuList()->where('menu_id', $menu->menu_id)->first();

        return response()->json([
            'isResponse' => true,
            'data' => $menu_list,
            'message' => 'Menu Updated Successfully'
        ]);
    }
    /**
     * @param $menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDelete($menu_id)
    {
        Menu::findOrFail($menu_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'Menu Information Deleted Successfully'
        ]);
    }
}
