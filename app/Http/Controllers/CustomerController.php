<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomers()
    {
        $customers = Customer::get();
        return response()->json([
            'isResponse' => true,
            'customers_info' => $customers
        ]);
    }

    /**
     * @param CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateCustomer(CustomerRequest $request)
    {
        $customer = Customer::create($request->all());

        return response()->json([
            'isResponse' => true,
            'data' => $customer,
            'message' => 'New Customer Saved Successfully'
        ]);
    }

    /**
     * @param $customer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdateCustomer($customer_id)
    {
        $customer = Customer::where('customer_id', $customer_id)->first();
        $renderHTML = view('dashboard.customer.customer-update', compact('customer'))->render();
        return response()->json([
            'isResponse' => true,
            'data' => $renderHTML
        ]);
    }

    /**
     * @param CustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateCustomer(CustomerRequest $request)
    {
        $customer_info = Customer:: findOrFail($request->customer_id);
        $customer_info->first_name    =  $request->first_name;
        $customer_info->last_name    =  $request->last_name;
        $customer_info->address      =  $request->address;
        $customer_info->city         =  $request->city;
        $customer_info->mobile       =  $request->mobile;
        $customer_info->phone        =  $request->phone;
        $customer_info->email        =  $request->email;
        $customer_info->save();

        return response()->json([
            'isResponse' => true,
            'data' => $customer_info,
            'message' => 'Customer Information Update Successfully'
        ]);
    }

    /**
     * @param $customer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeleteCustomer($customer_id)
    {
        Customer::findOrFail($customer_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'Customer Information Deleted Successfully'
        ]);
    }
}
