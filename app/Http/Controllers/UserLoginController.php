<?php

namespace App\Http\Controllers;

use App\Http\HelperModules\FileModules;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserLoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('auth/login');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user_name_type = 'cell_phone';
        //check username is an email
        if(filter_var($request->username, FILTER_VALIDATE_EMAIL))
            $user_name_type = 'email';

        if (Auth::attempt([$user_name_type => $request->username, 'password' => $request->password], $request->remember)) {
            return redirect()->route('adminIndex');
        }

        return redirect()->back()->withError('Invalid username or password');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserProfile()
    {
        return response()->json([
            'isResponse'    => true,
            'data'          => Auth::user()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUserProfile(Request $request)
    {
        Log::info($request->all());
        //for update password
        if($request->has('confirm_password')){
            return $this->password_changed($request);
        }
        //for update profile picture
        if($request->hasFile('picture')){
            return $this->postUserProfilePicture($request);
        }
        if(empty($request->first_name) || empty($request->last_name))
            return response()->json([
                'isResponse' => false,
                'message'    => 'Some Fields Are Missing'
            ]);
        User::where('id', Auth::user()->id)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name
            ]);
        return response()->json([
            'isResponse' => true,
            'message'    => 'Information Update Successfully'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function postUserProfilePicture($request)
    {
        $file = $request->picture;
        $type = $file->getClientOriginalExtension();
        if($type != 'jpg' && $type != 'png')
            return response()->json([
                'isResponse' => false,
                'message' => 'Please select a valid image'
            ]);

        $path = 'dashboard/uploads/profile-pictures/users/';
        $folderName = Auth::user()->id;
        $fileHelperModule = new FileModules();
        $image_name = $fileHelperModule->FileUpload($file, $path,$folderName);
        $user = User::find(Auth::user()->id);
        $user->profile_image = $image_name;
        $user->save();

        return response()->json([
            'isResponse' => true,
            'message' => 'Profile picture upload successfully'
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function password_changed($request)
    {
        if($request->password != $request->confirm_password)
            return response()->json([
                'isResponse' => false,
                'message'    => 'Passwords do not match'
            ]);
        User::where('id', Auth::user()->id)
            ->update([
                'password' => bcrypt($request->password)
            ]);
        return response()->json([
            'isResponse' => true,
            'message'    => 'Password Update Successfully'
        ]);
    }
}
