<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class PurchaseController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts()
    {
        $products = Product::get();
        return response()->json([
            'isResponse' => true,
            'products_info' => $products
        ]);
    }

}
