<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierRequest;
use App\Models\Supplier;

class SupplierController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuppliers()
    {
        $suppliers = Supplier::get();
        return response()->json([
            'isResponse' => true,
            'suppliers_info' => $suppliers
        ]);
    }

    /**
     * @param SupplierRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateSuppliers(SupplierRequest $request)
    {
         $supplier = Supplier::create($request->all());

        return response()->json([
            'isResponse' => true,
            'data' => $supplier,
            'message' => 'New Supplier Saved Successfully'
        ]);
    }

    /**
     * @param $supplier_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdateSupplier($supplier_id)
    {
        $supplier = Supplier::where('supplier_id', $supplier_id)->first();
        $renderHTML = view('dashboard.supplier.supplier-update', compact('supplier'))->render();
        return response()->json([
            'isResponse' => true,
            'data' => $renderHTML
        ]);
    }

    /**
     * @param SupplierRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateSupplier(SupplierRequest $request)
    {
        $supplier_info = Supplier:: findOrFail($request->supplier_id);
        $supplier_info->first_name    =  $request->first_name;
        $supplier_info->last_name    =  $request->last_name;
        $supplier_info->address      =  $request->address;
        $supplier_info->city         =  $request->city;
        $supplier_info->mobile       =  $request->mobile;
        $supplier_info->phone        =  $request->phone;
        $supplier_info->email        =  $request->email;
        $supplier_info->save();

        return response()->json([
            'isResponse' => true,
            'data' => $supplier_info,
            'message' => 'Supplier Information Update Successfully'
        ]);
    }

    /**
     * @param $supplier_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeleteSupplier($supplier_id)
    {
        Supplier::find($supplier_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'Supplier Information Deleted Successfully'
        ]);
    }
}
