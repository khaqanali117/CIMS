<?php

namespace App\Http\Controllers;

use App\Http\HelperModules\DateModules;
use App\Http\HelperModules\ProductModule;
use App\Http\Requests\DealsRequest;
use App\Models\Deals;
use App\Models\DealsComponents;
use App\Models\DealsSchedule;
use App\Models\Menu;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class DealsController extends Controller
{
    protected $dealsObj;

    /**
     * DealsController constructor.
     */
    public function __construct()
    {
        $this->dealsObj = new Deals();
        $this->dateModuleObj = new DateModules();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function dealsList()
    {
        $deals = $this->dealsObj->deals()
            ->select(['deal_id', 'deal_name', 'sale_price', 'description', 'created_by'])
            ->get();
        return response()->json([
            'isResponse' => true,
            'data' => $deals
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function productMenu()
    {
        $productModuleObj = new ProductModule();
        $allItems = $productModuleObj->productMenu();

        return response()->json([
            'isResponse' => true,
            'data' => $allItems
        ]);
    }

    /**
     * @param DealsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateDeal(DealsRequest $request)
    {
        /*save deal basic information*/
        $request['created_by'] = Auth::user()->id;
        $new_deal = Deals::create($request->all());
        /*save deal schedule information*/
        for ($i=0;$i< count($request->deal_start_time); $i++){
            if($request->deal_start_time[$i] && $request->deal_start_date[$i]){
                DealsSchedule::create([
                    'deal_id'           =>$new_deal->deal_id,
                    'deal_start_time'   => $request->deal_start_time[$i],
                    'deal_end_time'     => $request->deal_end_time[$i]?$request->deal_end_time[$i]:Null,
                    'deal_start_date'   => $request->deal_start_date[$i],
                    'deal_end_date'     => $request->deal_end_date[$i]?$$request->deal_end_date[$i]:Null,
                ]);
            }
        }
        /*save deal component information*/
        for ($i=0;$i< count($request->item_id); $i++){
            if($request->item_id[$i] && $request->quantity[$i]){
                $explode = explode('-',$request->item_id[$i]);
                $id = $explode[0];
                $type = $explode[1];
                DealsComponents::create([
                    'deal_id'       => $new_deal->deal_id,
                    $type           => (int)$id,
                    'quantity'      => $request->quantity[$i]
                ]);
            }
        }

        return response()->json([
            'isResponse' => true,
            'message' => Lang::get('deals/message.success.new_deal')
        ]);
    }

    /**
     * @param $deal_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdateDeal($deal_id)
    {
        $deal = $this->dealsObj->deals()->DealId($deal_id)->first();
        $allItems = $this->productMenu();
        $allItems = json_decode($allItems->content());
        return response()->json([
            'isResponse' => true,
            'items' => $allItems->data,
            'data' => $deal
        ]);
    }

    /**
     * @param DealsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateDeal(DealsRequest $request)
    {
        /*update deal basic information*/
        Deals::DealId($request->deal_id)
            ->update([
            'deal_name'     => $request->deal_name,
            'sale_price'    => $request->sale_price,
            'description'   => $request->description,
            'created_by'    => Auth::user()->id
        ]);
        /*delete deal schedule information*/
        DealsSchedule::where('deal_id', $request->deal_id)->delete();
        /*save deal schedule information*/
        for ($i=0;$i< count($request->deal_start_time); $i++){
            if($request->deal_start_time[$i] && $request->deal_start_date[$i]){
                DealsSchedule::create([
                    'deal_id'           => $request->deal_id,
                    'deal_start_time'   => $request->deal_start_time[$i],
                    'deal_end_time'     => $request->deal_end_time[$i]?$request->deal_end_time[$i]:Null,
                    'deal_start_date'   => $request->deal_start_date[$i],
                    'deal_end_date'     => $request->deal_end_date[$i]?$$request->deal_end_date[$i]:Null,
                ]);
            }
        }
        /*delete deal component information*/
        DealsComponents::where('deal_id', $request->deal_id)->delete();
        /*save deal component information*/
        for ($i=0;$i< count($request->item_id); $i++){
            if($request->item_id[$i] && $request->quantity[$i]){
                $explode = explode('-',$request->item_id[$i]);
                $id = $explode[0];
                $type = $explode[1];
                DealsComponents::create([
                    'deal_id'       => $request->deal_id,
                    $type           => (int)$id,
                    'quantity'      => $request->quantity[$i]
                ]);
            }
        }

        return response()->json([
            'isResponse' => true,
            'message' => Lang::get('deals/message.success.update_deal')
        ]);
    }
    /**
     * @param $deal_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeleteDeal($deal_id)
    {
        /*delete deal information*/
        Deals::findOrFail($deal_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => Lang::get('deals/message.success.delete_deal')
        ]);
    }
}
