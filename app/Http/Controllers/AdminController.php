<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Roles;
use App\Models\User;
use App\Models\UserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        $roles = Roles::get();
        $allUsers = User::with('userRoles')->get();
        return response()->json([
            'isResponse' => true,
            'user_info' => $allUsers,
            'user_roles' => $roles
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCreateUser()
    {
        $roles = Roles::get();
//        $renderHtml = view('dashboard/user/new-user', compact('roles'))->render();

        return response()->json([
            'isResponse' => true,
            'data' => $roles
        ]);
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCreateUser(UserRequest $request)
    {
        $request['password'] = bcrypt($request->password);
        $request['created_by'] = Auth::user()->id;
        $userInfo = User::create($request->all());
        UserRoles::create([
            'user_id' => $userInfo->id,
            'role_id' => $request->role_id,
        ]);
        //get new inserted user
        $user = User::with('userRoles')->where('id', $userInfo->id)->first();
        return response()->json([
            'isResponse' => true,
            'data' => $user,
            'message' => 'New User Saved Successfully'
        ]);
    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUpdateUser($user_id)
    {
        $roles = Roles::get();
        $user = User::with('user_role')->where('id', $user_id)->first();
        $renderHtml = view('dashboard/user/update-user', compact('roles', 'user'))->render();

        return response()->json([
            'isResponse' => true,
            'data' => $renderHtml
        ]);
    }

    /**
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdateUser(UserUpdateRequest $request)
    {
        User::where('id',$request->id)
            ->update([
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'password'      => bcrypt($request->password),
                'city'          => $request->city,
                'address'       => $request->address,
            ]);
        UserRoles::where('user_id',$request->id)->update(['role_id' => $request->role_id]);
        //get updated user
        $user = User::with('userRoles')->where('id', $request->id)->first();
        return response()->json([
            'isResponse' => true,
            'data' => $user,
            'message' => 'User Information Update Successfully'
        ]);
    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeleteUser($user_id)
    {
        User::find($user_id)->delete();
        return response()->json([
            'isResponse' => true,
            'message' => 'User Information Deleted Successfully'
        ]);
    }
}
