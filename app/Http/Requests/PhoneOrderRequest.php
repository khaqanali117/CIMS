<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class PhoneOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_no'   => 'required',
            'customer_name' => 'required',
            'address'       => 'required',
            'data'          => 'required',
            'total_price'   => 'required|greater_then'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'customer_no.required'      => Lang::get('generalMessages.error.required', ['name' => 'Customer No']),
            'customer_name.required'    => Lang::get('generalMessages.error.required', ['name' => 'Customer Name']),
            'address.required'          => Lang::get('generalMessages.error.required', ['name' => 'Customer Address']),
            'data.required'             => Lang::get('generalMessages.error.required', ['name' => 'One item']),
            'total_price.required'      => Lang::get('generalMessages.error.required', ['name' => 'One item']),
            'total_price.greater_then'  => Lang::get('generalMessages.error.required', ['name' => 'One item'])
        ];
    }

    /**
     * @param Validator $validator
     * @return mixed
     */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
