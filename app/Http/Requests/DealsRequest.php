<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class DealsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deal_name' => 'required',
            'sale_price' => 'required'
        ];
    }
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'deal_name.required'   => Lang::get('deals/message.required.deal_name'),
            'sale_price.required'   => Lang::get('deals/message.required.sale_price')
        ];
    }

    /**
     * @param Validator $validator
     * @return mixed
     */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
