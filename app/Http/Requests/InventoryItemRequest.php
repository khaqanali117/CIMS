<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class InventoryItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name'  => 'required',
            'quantity'      => 'required|integer',
            'min_stock_level'   => 'integer',
            'cost_price'    => 'required|integer',
            'sale_price'    => 'required|integer',
            'purchase_date' => 'required',
            'expiry'        => 'required',
            'product_type'  => 'required|integer',
            'company_id'    => 'required|integer',
            'supplier_id'   => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'product_name.required' => 'Product name is required',
            'quantity.required'     => 'Quantity is required',
            'quantity.integer'      => 'Quantity must be integer',
            'min_stock_level.integer' => 'Low Inventory Reminder must be integer',
            'cost_price.required'   => 'Cost price is required',
            'cost_price.integer'    => 'Cost price must be integer',
            'sale_price.required'   => 'Sale price is required',
            'sale_price.integer'    => 'Sale price must be integer',
            'purchase_date.required'=> 'Purchase date is required',
            'purchase_date.date_format'    => 'Purchase date must be formatted',
            'expiry.required'       => 'Expiry date is required',
            'expiry.date_format'           => 'Expiry date must be formatted',
            'product_type.required' => 'Product category is required',
            'company_id.required'   => 'Product company is required',
            'supplier_id.required'  => 'Product supplier is required',


        ];
    }
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
