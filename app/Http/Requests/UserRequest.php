<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|unique:users',
            'password'      => 'required',
            'cell_phone'    => 'required',
            'role_id'       => 'required',
            'address'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required'   => 'First name is required',
            'last_name.required'    => 'Last name is required',
            'email.required'        => 'Email is required',
            'email.unique'          => 'Email already used',
            'password.required'     => 'Password is required',
            'cell_phone.required'   => 'Contact no is required',
            'role_id.integer'       => 'Please define one of the listed Role',
            'address.required'      => 'Address is required'
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
