<?php
/**
 * User: khakan
 * Date: 24/06/16
 * Time: 15:07
 */

namespace App\Http\HelperModules;


class HelperModules
{
    /**58342
     * Response json
     * @param $response
     * @param $status
     * @param bool $token
     * @param bool $message
     * @param bool $responseData
     * @param bool $alert
     * @return \Symfony\Component\HttpFoundation\Response
     */
    static function responseJson($response, $status, $token=false, $message=false, $responseData=false, $alert = false)
    {
        $data = ['isResponse' => $response];
        $data['status'] = $status;
        if($token)
            $data['token'] = $token;
        if($message)
            $data['message'] = $message;
        if($responseData)
            $data['data'] = $responseData;
        if($alert)
            $data['alert'] = $responseData;
        return response()->json($data);
    }

}