<?php
/**
 * Created by PhpStorm.
 * User: khakan
 * Date: 24/06/16
 * Time: 15:07
 */

namespace App\Http\HelperModules;


use file;

class FileModules implements file
{

    public function FileUpload($file,$path,$folderName)
    {
        $path = $path.$folderName;
        $this->make_dir($path,0777);
        $destination_folder = base_path('public/'.$path.'/');
        $image_name = $file->getClientOriginalName();
        $file->move($destination_folder,$image_name);
        return $image_name;
    }

    /** -- Make directory --
     * @param $path
     * @param $permission
     * @return bool
     */
    public function make_dir($path,$permission){
        if(file_exists($path))
            return true;
        return mkdir($path,$permission);
    }

}