<?php

namespace App\Http\HelperModules;

use App\Models\Deals;
use App\Models\Menu;
use App\Models\Product;
use App\Models\Stock;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProductModule
{
    /**
     * @return array
     */
    public function productMenu()
    {
        $allProducts = Product::select(['product_id', 'product_name'])->get()->toArray();
        $allMeans   = Menu::select(['menu_id', 'title'])->get()->toArray();

        $allItems = array_merge($allProducts, $allMeans);

        return $allItems;
    }

    /**
     * @return array
     */
    public function productMenuDeal()
    {
        $this->stock = new Stock();
        $productSelect = [
            'product_id',
            DB::raw('product_name as name'),
            'discount',
            'cost_price',
            'sale_price'
        ];
        $menuSelect = [
            'menu_id',
            DB::raw('title as name'),
            'sale_price'
        ];
        $dealSelect = [
            'deal_id',
            DB::raw('deal_name as name'),
            'sale_price'
        ];
        $allProducts = $this->stock->product_info($productSelect)
            ->where('quantity','>', 0)
            ->whereDate('expiry','>=',Carbon::now()->toDateString())
            ->orderBy('expiry')
            ->get()
            ->toArray();
        $allMenus = Menu::select($menuSelect)
            ->get()
            ->toArray();
        $allDeals = Deals::select($dealSelect)
            ->get()
            ->toArray();

        $Items = array_merge($allProducts, $allMenus);
        $allItems = array_merge($Items, $allDeals);
        return $allItems;

    }
}
