<?php
/**
 * Created by PhpStorm.
 * User: khakan
 * Date: 24/06/16
 * Time: 15:07
 */

namespace App\Http\HelperModules;


use Carbon\Carbon;

class DateModules
{

    public function dateConvert($date)
    {
        return date('Y-m-d',strtotime(str_replace('/', '-', $date)));
    }

    public function timeConvert($time)
    {
        return date('H:i:s',strtotime($time));
    }
    #format time Am and Pm
    public function timeDisplayFormat($time){
        return date('h:i A',strtotime($time));
    }
    public function dateTimeConvert($dateTime)
    {
        return date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $dateTime)));
    }

    public function dateConvertToDateTimePicker($date)
    {
        return date('d/m/Y',strtotime($date));
    }
}