<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;

    public $table = 'product_category';
    public $primaryKey = 'category_id';
    public $fillable = [
        'category_name',
        'description'
    ];
}
