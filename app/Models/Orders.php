<?php

namespace App\Models;

use App\Http\HelperModules\DateModules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Orders extends Model
{
    public $table = 'orders';
    public $primaryKey = 'order_id';
    public $fillable = [
        'customer_id',
        'sale_price',
        'order_discount',
        'payment_received',
        'order_type',
        'order_date',
        'table_no',
        'created_by',
        'order_status'
    ];
    /**
     * @param $value
     * @return string
     */
    public function getOrderDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $dateModuleObj->dateConvertToDateTimePicker($value);
    }
    /**
     * @param $value
     * @return string
     */
    public function getTableNoAttribute($value)
    {
        if($value)
            return $value;
        else
            return "Null";
    }
    /**
     * @param $value
     * @return string
     */
    public function getOrderTypeAttribute($value)
    {
        if($value == 1)
            return "Phone Order";
        else
            return "Indoor Order";
    }
    /**
     * @param $value
     * @return string
     */
    public function getOrderStatusAttribute($value)
    {
        if($value == 0)
            return "Pending";
        else if($value == 1)
            return "Confirm";
        else
            return "Cancel";
    }

    /**
     * @param string $select
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function ordersList($select = '*')
    {
        $Orders = DB::table('orders');
        /* select columns*/
        $Orders->select($select);
        /* get all orders items*/
        $Orders->join('order_items', function ($query){
            $query->on('order_items.order_id', 'orders.order_id');
        });
        /* get customer info*/
        $Orders->join('customers', function ($query){
            $query->on('customers.customer_id', 'orders.customer_id');
        });
        /* Left Join with stock table*/
        $Orders->leftJoin('stock', function ($query){
            $query->on('stock_id', 'item_id')
                ->where('item_type', 'product');
        });
        /* Left Join with product table*/
        $Orders->leftJoin('product', function ($query){
            $query->on('stock.product_id', 'product.product_id');
        });
        /* Left Join with menu table*/
        $Orders->leftJoin('menu', function ($query){
            $query->on('menu_id', 'item_id')
                ->where('item_type', 'menu');
        });
        /* Left Join with deal table*/
        $Orders->leftJoin('deals', function ($query){
            $query->on('deal_id', 'item_id')
                ->where('item_type', 'deal');
        });

        return $Orders;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_item()
    {
        return $this->hasMany('App\Models\OrderItems', 'order_id', 'order_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer_info()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'customer_id');
    }

    public function user_info()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
}
