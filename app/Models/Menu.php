<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    public $table = 'menu';
    public $primaryKey = 'menu_id';
    public $fillable = [
        'title',
        'description'
    ];

    public static function MenuList()
    {
        return self::with('menu_component.product_info');
    }

    public function menu_component()
    {
        return $this->hasMany('App\Models\MenuComponent','menu_id','menu_id');
    }
}
