<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DealsComponents extends Model
{
    use SoftDeletes;

    public $table = 'deal_components';
    public $primaryKey = 'deal_component_id';
    public $fillable = [
        'product_id',
        'deal_id',
        'menu_id',
        'quantity'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id','product_id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu','menu_id','menu_id');
    }
}
