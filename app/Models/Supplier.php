<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    public $table = 'supplier';
    public $primaryKey = 'supplier_id';
    public $fillable = [
        'first_name',
        'last_name',
        'address',
        'city',
        'mobile',
        'phone',
        'email'
    ];
}
