<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'product';
    public $primaryKey = 'product_id';

    public $fillable = [
        'product_name',
        'cost_price',
        'sale_price',
        'product_type',//category
        'min_stock_level',//alert
        'description',
    ];

    /**
     * @param string $select
     * @return mixed
     */
    public function product_info($select = '*'){
        return $this->whereHas('stock',function ($query){
            $query->whereDate('expiry','>=',Carbon::now()->toDateString());
        })->with('stock')->select($select);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stock()
    {
        return $this->hasMany('App\Models\Stock','product_id','product_id');
    }

    public function product_category()
    {
        return $this->belongsTo('App\Models\ProductCategory','product_type','category_id');
    }
}
