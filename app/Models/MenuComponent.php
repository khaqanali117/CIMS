<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuComponent extends Model
{
    use SoftDeletes;

    public $table = 'menu_components';
    public $primaryKey = 'id';
    public $fillable = [
        'menu_id',
        'product_id',
        'quantity'
    ];

    public function product_info()
    {
        return $this->belongsTo('App\Models\product', 'product_id', 'product_id');
    }
}
