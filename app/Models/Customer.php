<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';
    public $primaryKey = 'customer_id';
    public $fillable = [
        'first_name',
        'last_name',
        'address',
        'city',
        'mobile',
        'phone',
        'email'
    ];
}
