<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    public $table = 'order_items';
    public $primaryKey = 'order_item_id';
    public $fillable = [
        'order_id',
        'item_id',
        'item_type',
        'item_name',
        'sale_price',
        'quantity',
        'item_discount'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'item_id', 'product_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'item_id', 'menu_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deal()
    {
        return $this->belongsTo('App\Models\Deals', 'item_id', 'deal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo('App\Models\Stock', 'item_id', 'stock_id');
    }
}
