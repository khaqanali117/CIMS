<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deals extends Model
{
    use SoftDeletes;

    public $table = 'deals';
    public $primaryKey = 'deal_id';
    public $fillable = [
        'deal_name',
        'sale_price',
        'description',
        'created_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function deals()
    {
        return $this->with([
            'deal_components',
            'deal_components.product' => function($query){
                $query->select(['product_id', 'product_name', 'sale_price']);
            }, 'deal_components.menu' => function($query){
                $query->select(['menu_id', 'title', 'sale_price']);
            }, 'deal_schedule', 'user_info' => function($query){
                $query->select(['id', 'first_name', 'last_name']);
            }]);
    }
    /**
     * deal id local scope
     */
    public function scopeDealId($query, $value)
    {
        return $query->where('deal_id', $value);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deal_schedule()
    {
        return $this->hasMany('App\Models\DealsSchedule','deal_id', 'deal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deal_components()
    {
        return $this->hasMany('App\Models\DealsComponents','deal_id','deal_id');
    }

    public function user_info()
    {
        return  $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
}
