<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCompany extends Model
{
    public $table = 'product_company';
    public $primaryKey = 'pcompany_id';
    public $fillable = [
        'stock_id',
        'company_id'
    ];
}
