<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\HelperModules\DateModules;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;
    public $table = 'stock';
    public $primaryKey = 'stock_id';
    public $fillable = [
        'product_id',
        'batch_no',
        'quantity',
        'expiry',
        'purchase_date'
    ];

    /**
     * @param string $select
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function product_info($select = '*'){
        return $this->with(['product' => function($query) use($select){
            $query->select($select);
        },'product.product_category','stock_company','stock_supplier']);
            //->whereDate('expiry','>=',Carbon::now()->toDateString());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id','product_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stock_company()
    {
        return $this->belongsToMany('App\Models\Company','product_company','stock_id','company_id');
    }

    public function stock_supplier()
    {
        return $this->belongsToMany('App\Models\Supplier','product_supplier','stock_id','supplier_id');
    }

    /*public function order_items()
    {
        return $this->hasOne('App\Models\OrderItems', 'item_id', 'stock_id');
    }*/
    /**
     * Defining A Mutator
     * @param $value
     */
    public function setPurchaseDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        $this->attributes['purchase_date'] = $dateModuleObj->dateConvert($value);
    }

    /**
     * Defining A Mutator
     * @param $value
     */
    public function setExpiryAttribute($value)
    {
        $dateModuleObj = new DateModules();
        $this->attributes['expiry'] = $dateModuleObj->dateConvert($value);
    }

    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getPurchaseDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $dateModuleObj->dateConvertToDateTimePicker($value);
    }

    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getExpiryAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $dateModuleObj->dateConvertToDateTimePicker($value);
    }
}
