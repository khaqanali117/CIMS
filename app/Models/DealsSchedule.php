<?php

namespace App\Models;

use App\Http\HelperModules\DateModules;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class DealsSchedule extends Model
{
    use SoftDeletes;

    public $table = 'deals_schedule';
    public $primaryKey = 'deal_schedule_id';
    public $fillable = [
        'deal_id',
        'deal_start_time',
        'deal_end_time',
        'deal_start_date',
        'deal_end_date'
    ];

    /**
     * Defining A Mutator
     * @param $value
     */
    public function setDealStartTimeAttribute($value)
    {
        Log::info($value);
        $dateModuleObj = new DateModules();
        $this->attributes['deal_start_time'] = $value?$dateModuleObj->timeConvert($value):Null;
    }

    /**
     * Defining A Mutator
     * @param $value
     */
    public function setDealEndTimeAttribute($value)
    {
        $dateModuleObj = new DateModules();
        $this->attributes['deal_end_time'] = $value?$dateModuleObj->timeConvert($value):Null;
    }

    /**
     * Defining A Mutator
     * @param $value
     */
    public function setDealStartDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        $this->attributes['deal_start_date'] = $value?$dateModuleObj->dateConvert($value):Null;
    }

    /**
     * Defining A Mutator
     * @param $value
     */
    public function setDealEndDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        $this->attributes['deal_end_date'] = $value?$dateModuleObj->dateConvert($value):Null;
    }

    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getDealStartTimeAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $value?$dateModuleObj->timeDisplayFormat($value):null;
    }
    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getDealEndTimeAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $value?$dateModuleObj->timeDisplayFormat($value):null;
    }
    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getDealStartDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $value?$dateModuleObj->dateConvertToDateTimePicker($value):null;
    }
    /**
     * Defining A Accessor
     * @param $value
     * @return bool|string
     */
    public function getDealEndDateAttribute($value)
    {
        $dateModuleObj = new DateModules();
        return $value?$dateModuleObj->dateConvertToDateTimePicker($value):null;
    }

}
