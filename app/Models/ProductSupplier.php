<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSupplier extends Model
{
    public $table = 'product_supplier';
    public $primaryKey = 'psupplier_id';

    public $fillable = [
        'stock_id',
        'supplier_id',
    ];
}
