<?php
/**
 * Created by Khakan Ali
 */

interface file{

    /**
     * @param $file
     * @param $path
     * @param $folderName
     * @return mixed
     */
    public function FileUpload($file, $path, $folderName);

    /**
     * @param $path
     * @param $permission
     * @return mixed
     */
    public function make_dir($path, $permission);
}